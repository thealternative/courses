# instantiate project
copy the Makefile and example.typ to a subdirectory
you'll need the font "Latin Modern" in order to get the correct look

# output pdf
use `m̀ake` to build
use `make watch` to start watching and continuously compiling the file

# change the file being compiled / add more

modify the Makefile put all targets in the TARGET variable
