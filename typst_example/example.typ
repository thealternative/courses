#import "@preview/touying:0.5.3": *
#import "/typst_template/thealternative.typ": *


#show: thealt-theme.with(
  aspect-ratio: "16-9",
  config-info(
    title: [Our epic typst talk],
    subtitle: [Subtitle],
    author: [Fadri Lardon, Johanna Polzin],
    date: datetime.today(),
    institution: [Institution],
  ),
)
#show: codly-init.with()

#title-slide()

= I'm a section
== this is an example slide
Hello
=== subsubsection go

```C
# include <iostream>

using namespace std;

int main () {
    const int N = 10; // larger N for slower learners
    for(int i = 0; i < N; i++) {
        cout << "FOSS is also great for non-programmers!" << endl;
    }
    return 0;
}```

=== subsubsection gogo

```bash
echo "Hello"
```
=== subsubsection gogogo

== another example slide
Hello too

= A second section
== hello
#lorem(80)
=== AAAH

