# Git and GitLab Exercise Sheet Solutions

These are the solutions to the Git and GitLab exercise sheet. Try to solve the git and gitlab exercise sheet on your own. If you need help you can of course check with the solutions.

## Creating a repository

Go to your GitLab and create a new project. Fill out the form:

![GitLab Create Project](../../../assets/img/gitlab_create_project.png)

Copy the ssh link from the "code" dropdown menu

![GitLab SSH Link](../../../assets/img/gitlab_clone_ssh.png)

Go into a directory you want to clone the project and run the command

```
git clone the_ssh_link
```

## Making changes

### Create a commit

Add a file to your project.

Run "git status" and see that your newly created file is mentioned in the git status.

Commit by running

```
git add .
git commit -m "My first commit"
```

Observe in the "git log" that the new commit is added.

To upload the commit by running "git push"

Then go to your repository in gitlab and view your changes there.

Click on a file and edit it. Commit the file directly in the webinterface of gitlab. (Note: This is just for demostration purposed. While it is possible, the web editor is rarly used.)

Now, run locally "git pull" to update your local repository. Observe that the changes you made online are now also locally on your system.

Extras:

Write something in the file or create more files and commit them. Checkout to the different commits and see that all your changes are saved in snapshots.

### Branches

Run "git log" to get the commit hash of an older commit. These are the numbers in front of a commit.

Then run "git checkout commit_hash" and replace the "commit_hash" placeholder with the first few digits of the commit hash.

Observe in your file explorer that the changes have vanished.

Run "git checkout main" to get back to the newest version of the main branch.

Run

```
git checkout -b test
```

Add a file to the project and run

```
git add .
git commit -m "Commit on a different branch"
```

To switch back to the main branch, run

```
git checkout main
```

Add a new file to the project and commit the changes

```
git add .
git commit -m "A commit on main"
```

Switch between the test branch and the main branch by running

```
git checkout test
git checkout main
```

respectively and see the different changes are seperate from each other.

Upload the branch by running:

```
git push --set-upstream origin test
```

Then go to your repository on gitlab and view in the branch selection dropdown menu your newly created "test" branch.

## Merging

Checkout to the main branch by running

```
git checkout main
```

To merge the "test" branch into the "main" branch, run

```
git merge test
```

Observe that the "main" branch contains all changes now and the "test" branch has only the old changes.

### Merge Conflicts

Use the commands from above and follow the instructions from the above exercises. There is no new command used.

## .gitignore

Create a ".gitignore" file with the command "touch .gitignore" command.

Create another file called "please_ignore_me.txt"

Check that git status recognizes the file as a change

Write the name of the "please_ignore_me.txt" in the ".gitignore" file.

Check that git status doesn't recognizes the file as a change anymore.

Commit the changes and play around with the "please_ignore_me.txt". Observe that git doesn't protect the file anymore.
