# Git and GitLab Exercise Sheet

These are some exercises complementing the git and gitlab beginner course.

Explanations on how to solve this exercises can be read from the slides or through small help from google.

<!-- ## Installation & Setup -->

<!-- In this exercise section we are going to give you some guidance exercises to setup git and learn to use the command line. -->

<!-- ### Install Git -->

<!-- In this exercise we want to install git. -->

<!-- 1. Go to the website https://git-scm.com/downloads and install git for your operating system. -->
<!-- 2. On Windows: Open Git Bash -->
   <!-- On Mac/Linux: Open a Command Line and check if the command "git" is recognized. The output should be large usage explanation -->

<!-- ### Setup Git -->

<!-- Run the configuration commands -->

<!-- ```bash -->
<!-- git config --global core.editor "nano" -->
<!-- git config --global user.email "your email" -->
<!-- git config --global user.name "your account name" -->
<!-- git config --global pull.rebase false -->
<!-- ``` -->

<!-- ### Setup GitLab -->

<!-- 1. Log into your GitLab -->
<!-- 2. Go to the ssh key settings -->
<!-- 3. Generate an SSH Key with -->

<!-- ```bash -->
<!-- ssh-keygen -t ed25519 -->
<!-- # 1. For the first question just leave -->
<!-- #    it empty and press enter -->

<!-- # 2. For the second question enter a passphrase -->
<!-- #    for the key -->

<!-- # Sometimes you need to execute this command. -->
<!-- # Try and if it fails ignore it. -->
<!-- ssh-add ~/.ssh/id_ed25519 -->
<!-- ``` -->

<!-- 4. Get the public key by running -->

<!-- ```bash -->
<!-- # Get the public key -->
<!-- cat .ssh/id_ed25519.pub -->
<!-- ``` -->

<!-- 5. Copy the key into gitlab. -->

## Creating a repository

1. Create a repository on GitLab
2. Clone the repository from GitLab

## Making changes

Note: Observe means that you can simply view the files in your normal file explorer

### Create a commit

1. Make some changes in your repository
2. Observe the uncommited changes in the "git status"
3. Commit the changes
4. Observe the commit in "git log"
5. Upload the changes to GitLab
6. Observe your changes on GitLab
7. Make a change **on GitLab** to a file
8. Pull the changes from GitLab

Extras:

4. Create some more commits and observe that all the changes are saved in the different commits

### Branches

1. Observe that if you checkout to an older commit, the changes vanish
2. Checkout back to main
3. Create a branch called "test"
4. Make some changes (in a different file) to the branch "test"
5. Switch to the "main" branch and make some changes there.
6. Observe that now the "test" branch and the "main" branch have two different versions of your project.
7. Upload the "test" branch to GitLab
8. Observe the changes of the "test" branch on GitLab

## Merging

1. Go to the "main" branch and merge the "test" branch into the "main" branch.
2. Observe that "main" carries all the changes
3. Observe that the "test" branch only has its changes

### Merge Conflicts

1. Add a file called "myconflict.txt" to the "main" branch and write a simple line into it
2. Add the same file to the "test" branch and write a different line into it
3. Go to the "main" branch and try to merge the "test" branch into the "main" branch
4. Observe that the merge fails and figure out a reason why it fails.
5. Observe the conflict markers in the "myconflict.txt" file.
6. Resolve the conflict by choosing one version in the "myconflict.txt" file.
7. Stage and commit the file and hence conclude the merge conflict.
8. Observe that we have now successfully merged "test" into "main" as we intended.

## .gitignore

1. Create a ".gitignore" file (might need to be created through the command "touch .gitignore")
2. Create a new file "please_ignore_me.txt"
3. Observe with git status that git cares about the newly created file
4. Write into the ".gitignore" file "please_ignore_me.txt"
5. Observe that git status shows that git is now ignoring the file
6. Commit the ".gitignore" file.
7. Change the "please_ignore_me.txt" and switch to an older commit
8. Observe that there are no changes to commit.
9. Observe that the "please_ignore_me.txt" file still has the newest changes.
