# Git Crash Course Exercise Sheet

These are some exercises complementing the Git Crash Course.

Explanations on how to solve this exercises can be read from the slides or through small help from google.

## Installation & Setup

In this exercise section we are going to give you some guidance exercises to setup git and learn to use the command line.

### Install Git

In this exercise we want to install git.

1. Go to the website https://git-scm.com/downloads and install git for your operating system.
2. On Windows: Open Git Bash
   On Mac/Linux: Open a Command Line and check if the command "git" is recognized. The output should be large usage explanation

### Setup Git

Run the configuration commands

```bash
git config --global core.editor "nano"
git config --global user.email "your email"
git config --global user.name "your account name"
git config --global pull.rebase false
```

### Setup GitLab

1. Log into your GitLab
2. Go to the ssh key settings
3. Generate an SSH Key with

```bash
ssh-keygen -t ed25519
# 1. For the first question just leave
#    it empty and press enter

# 2. For the second question enter a passphrase
#    for the key

# Sometimes you need to execute this command.
# Try and if it fails ignore it.
ssh-add ~/.ssh/id_ed25519
```

4. Get the public key by running

```bash
# Get the public key
cat .ssh/id_ed25519.pub
```

5. Copy the key into gitlab.

## Creating a repository

1. Create a repository on GitLab
2. Clone the repository from GitLab
3. (If you are on MacOS, create the `.gitignore` file)

## Making changes

Note: Observe means that you can simply view the files in your normal file explorer

## Create a commit

1. Make some changes in your repository
2. Observe the uncommited changes in the "git status"
3. Commit the changes
4. Observe the commit in "git log"
5. Upload the changes to GitLab
6. Observe your changes on GitLab
7. Make a change **on GitLab** to a file
8. Pull the changes from GitLab

Extras:

9. Create some more commits and observe that all the changes are saved in the different commits

### Checkout to old commits

1. Observe that if you checkout to an older commit, the changes vanish
2. Checkout back to main
