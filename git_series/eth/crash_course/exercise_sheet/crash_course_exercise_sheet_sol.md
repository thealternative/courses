# Git Crash Course Exercise Sheet Solutions

These are the solutions to the Git and GitLab exercise sheet. Try to solve the git and gitlab exercise sheet on your own. If you need help you can of course check with the solutions.

## Creating a repository

Go to your GitLab and create a new project. Fill out the form:

![GitLab Create Project](../../../assets/img/gitlab_create_project.png)

Copy the ssh link from the "code" dropdown menu

![GitLab SSH Link](../../../assets/img/gitlab_clone_ssh.png)

Go into a directory you want to clone the project and run the command

```
git clone the_ssh_link
```

## Making changes

### Create a commit

Add a file to your project.

Run "git status" and see that your newly created file is mentioned in the git status.

Commit by running

```
git add .
git commit -m "My first commit"
```

Observe in the "git log" that the new commit is added.

To upload the commit by running "git push"

Then go to your repository in gitlab and view your changes there.

Click on a file and edit it. Commit the file directly in the webinterface of gitlab. (Note: This is just for demostration purposed. While it is possible, the web editor is rarly used.)

Now, run locally "git pull" to update your local repository. Observe that the changes you made online are now also locally on your system.

Extras:

Write something in the file or create more files and commit them. Checkout to the different commits and see that all your changes are saved in snapshots.

### Checkout to old commits

Run "git log" to get the commit hash of an older commit. These are the numbers in front of a commit.

Then run "git checkout commit_hash" and replace the "commit_hash" placeholder with the first few digits of the commit hash.

Observe in your file explorer that the changes have vanished.

Run "git checkout main" to get back to the newest version of the main branch.
