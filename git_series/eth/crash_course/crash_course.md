---
author:
  - Ian Wasser
title: Git Crash Course
---

# Introduction

##

\includegraphics[width=0.5\textwidth]{../../assets/img/logo_blue.pdf}

\vspace{4cm}

\note[item]{Welcome to the talk!}
\note[item]{As you can see, this slidedeck is a work in progress.}

##

\includegraphics[width=0.5\textwidth]{../../assets/img/logo_blue.pdf}

Foss: Free and Open Source Software

\vspace{4cm}

##

\includegraphics[width=0.5\textwidth]{../../assets/img/logo_blue.pdf}

Foss: Free and Open Source Software

\includegraphics[height=4cm, right]{../../assets/img/git_logo.png}

## Organisational

- Slides on the website
- Questions at any time
- We have theoretical and practical time
- Next courses

## Organisational

- Installation
- Basic Concepts

<!--## Organisational

\begin{textblock}{3}(2, 5)
\includegraphics[width=3cm]{../../assets/img/qr_slides.png}
\begin{center}
\textbf{Slides}
\end{center}
\end{textblock}

\begin{textblock}{3}(6, 5)
\includegraphics[width=3cm]{../../assets/img/qr_exercise_sheet.png}
\begin{center}
\textbf{Exercise Sheet}
\end{center}
\end{textblock}

\begin{textblock}{3}(10, 5)
\includegraphics[width=3cm]{../../assets/img/qr_cheat_sheet.png}
\begin{center}
\textbf{Git Cheat Sheet}
\end{center}
\end{textblock}

-->

<!-- FIX: Warning these qr codes point to a branch -->

## What is Git?

\begin{center}
What is Git?
\end{center}

## What is Git?

\begin{center}
Git is the most popular \textbf{Source Control Software} out there
\end{center}

## Source Control

## Source Control

\begin{center}
\includegraphics[width=0.7\textwidth]{../../assets/img/git_usecase_1.png}
\end{center}

## Source Control

\begin{center}
\includegraphics[width=0.7\textwidth]{../../assets/img/git_usecase_2.png}
\end{center}

## Source Control

\begin{center}
\includegraphics[width=0.7\textwidth]{../../assets/img/git_usecase_3.png}
\end{center}

## Source Control

\begin{center}
\includegraphics[width=0.7\textwidth]{../../assets/img/git_usecase_4.png}
\end{center}

## Collaboration

## Short History

## Short History

\begin{center}
\includegraphics[width=0.7\textwidth]{../../assets/img/git_story_1.png}
\end{center}

## Short History

\begin{center}
\includegraphics[width=0.7\textwidth]{../../assets/img/git_story_2.png}
\end{center}

## Short History

\begin{center}
\includegraphics[width=0.7\textwidth]{../../assets/img/git_story_3.png}
\end{center}

## Short History

\begin{center}
\includegraphics[width=0.8\textwidth]{../../assets/img/git_story_4.png}
\end{center}

# Installation and Setup

##

\begin{center}
\textbf{Installation and Setup}
\end{center}

## Installation: Git

\begin{textblock}{5}(2, 6)
Go to the website

\textbf{https://git-scm.com/downloads}

and download git there.
\end{textblock}

\begin{center}
\includegraphics[width=5cm, right]{../../assets/img/git_website.png}
\end{center}

## Installation: Git on Mac

Installing Homebrew in the terminal

`/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`

Then run

`brew install git`

## Installation: Git on Linux

Install it with your package manager

Examples:

Ubuntu/Mint/Debian:

`sudo apt install git`

Fedora:

`sudo dnf install git`

Arch:

`sudo pacman -S git`

## Setup: Git

## Setup: Git

\begin{center}
\includegraphics[height=5cm]{../../assets/img/gitbash.png}

Git is a command line tool
\end{center}

## Setup: Git

\begin{center}
\includegraphics[height=5cm]{../../assets/img/gitkraken.png}
\end{center}

## Introduction into the Command Line

Don't panic

It is just different way to interact with the computer

## Opening the command line

Windows: Open the program "Git Bash"

Mac: Open the program called "Terminal"

Linux: Often also just called terminal

## Using the command line

Important part what is written at the place **~** -> Current directory

\begin{center}
\includegraphics[width=6cm]{../../assets/img/command_prefix_1.png}
\end{center}

## Using the command line

**~** is your home directory.

Windows: `C:Users/<username>`

Mac/Linux: `/home/<username>`

## Using the command line

\begin{textblock}{5}(1, 3)
\includegraphics[width=5cm]{../../assets/img/command_cd.png}
\end{textblock}

## Using the command line

Observe when you switch into another directory

\includegraphics[width=7cm]{../../assets/img/command_prefix_2.png}

## Using the command line

\begin{textblock}{5}(1, 3)
\includegraphics[width=5cm]{../../assets/img/command_cd.png}
\end{textblock}

\begin{textblock}{5}(9, 3)
\includegraphics[width=5cm]{../../assets/img/command_ls.png}
\end{textblock}

## Using the command line

\begin{textblock}{5}(1, 3)
\includegraphics[width=5cm]{../../assets/img/command_cd.png}
\end{textblock}

\begin{textblock}{5}(9, 3)
\includegraphics[width=5cm]{../../assets/img/command_ls.png}
\end{textblock}

\begin{textblock}{6}(6, 8)
\includegraphics[width=6cm]{../../assets/img/command_cat.png}
\end{textblock}

## Setup: Git

## Setup: Git

\begin{center}
\includegraphics[width=0.7\textwidth]{../../assets/img/command_git_config.png}
\end{center}

## Setup: GitLab

## Authentication Theory

\begin{center}
\includegraphics[width=0.8\textwidth]{../../assets/img/rsa_1.png}
\end{center}

## Authentication Theory

\begin{center}
\includegraphics[width=0.8\textwidth]{../../assets/img/rsa_2.png}
\end{center}

## Authentication Theory

\begin{center}
\includegraphics[width=0.8\textwidth]{../../assets/img/rsa_3.png}
\end{center}

## Authentication Theory

\begin{center}
Don't worry this is not that relevant for the usage of Git
\includegraphics[width=0.7\textwidth]{../../assets/img/rsa_3.png}
\end{center}


## Setup: GitLab

\begin{center}
Back into Git Bash run the following:

\includegraphics[width=0.4\textwidth]{../../assets/img/command_ssh_keygen.png}
\end{center}

The passphrase won't show anything. Even if you type. Trust me, it is typing.

## Setup: GitLab

\begin{center}
\includegraphics[height=0.7\textheight]{../../assets/img/command_cat_key.png}

Copy the keys CTRL + SHIFT + C

(Just CTRL + C/V doesn't work in the command line, you need a SHIFT)
\end{center}

## Setup: GitLab

Go to your gitlab instance and sign in with your account:

- \textbf{https://gitlab.ethz.ch}

## Setup: GitLab

\begin{center}
\includegraphics[height=0.7\textheight]{../../assets/img/gitlab_preferences.png}
\end{center}

## Setup: GitLab

\begin{center}
\includegraphics[height=0.7\textheight]{../../assets/img/gitlab_keys.png}
\end{center}

## Setup: GitLab

\begin{center}
Paste the key here

\includegraphics[height=0.7\textheight]{../../assets/img/gitlab_add_key.png}
\end{center}

##

\begin{center}
\textbf{We are ready!}
\end{center}

##

\begin{center}
\textbf{We are ready!}

Now, it's your turn
\end{center}

## Installation and Setup Summary Page:

- Go to \textbf{https://git-scm.com/downloads}
- Run the first command series on the right
- To to your gitlab page, login
- Go to the ssh key settings page
- Generate an SSH Key (second command)
- Copy the SSH Key (third command)
- Paste the key into GitLab

\begin{textblock}{5}(10, 2)
\includegraphics[width=5cm]{../../assets/img/command_git_config.png}
\end{textblock}

\begin{textblock}{5}(10, 4.5)
\includegraphics[width=5cm]{../../assets/img/command_ssh_keygen.png}
\end{textblock}

\begin{textblock}{5.8}(9.6, 9.2)
\includegraphics[width=5.8cm]{../../assets/img/command_cat_key.png}
\end{textblock}

# Concepts

##

\begin{center}
\textbf{Concepts}
\end{center}

## Repository

## Repository

\begin{center}
A repository is a git project
\end{center}

## Repository

\begin{center}
A repository is a git project

(which is just your project)
\end{center}

## Repository

\begin{center}
\includegraphics[width=0.8\textwidth]{../../assets/img/repo_files.png}
\end{center}

## Git History

\begin{center}
\includegraphics[width=0.8\textwidth]{../../assets/img/git_history_simple.png}
\end{center}

## Git Commit

\begin{center}
\includegraphics[width=0.8\textwidth]{../../assets/img/git_history_simple.png}
\end{center}

## Git Commit

\begin{center}
\includegraphics[width=0.8\textwidth]{../../assets/img/git_commit_folder_analogy.png}
\end{center}

## Git Commit

\begin{center}
\includegraphics[width=0.8\textwidth]{../../assets/img/git_commit_naming.png}
\end{center}

## Git Commit

\begin{center}
\includegraphics[width=0.8\textwidth]{../../assets/img/git_commit_checkout_1.png}
\end{center}

## Git Commit

\begin{center}
\includegraphics[width=0.8\textwidth]{../../assets/img/git_commit_checkout_2.png}
\end{center}

## Git Commit

\begin{center}
\includegraphics[width=0.8\textwidth]{../../assets/img/git_commit_checkout_3.png}
\end{center}

## Making Changes

\begin{center}
\includegraphics[width=0.8\textwidth]{../../assets/img/git_history_simple.png}
\end{center}

## Making Changes

\begin{center}
\includegraphics[width=0.8\textwidth]{../../assets/img/git_makingchanges_1.png}
\end{center}

## Making Changes

\begin{center}
\includegraphics[width=0.8\textwidth]{../../assets/img/git_makingchanges_2.png}
\end{center}

## Making Changes

\begin{center}
\includegraphics[width=0.8\textwidth]{../../assets/img/git_makingchanges_3.png}
\end{center}

## Making Changes

\begin{center}
\includegraphics[width=0.8\textwidth]{../../assets/img/git_makingchanges_4.png}
\end{center}

## Making Changes

\begin{center}
\includegraphics[width=0.8\textwidth]{../../assets/img/git_makingchanges_5.png}
\end{center}

## Making Changes

\begin{center}
Questions?
\end{center}

## GitLab

\begin{textblock}{1}(6, 8)
\textbf{GitLab}
\end{textblock}

\begin{center}
\includegraphics[width=5cm, right]{../../assets/img/gitlab_logo.png}
\end{center}

## Git vs. GitLab

## Git vs. GitLab

\begin{textblock}{5}(2, 4)
\begin{center}
\textbf{Git}

Application that manages your projects and enables source control
\end{center}
\end{textblock}

## Git vs. GitLab

\begin{textblock}{5}(2, 4)
\begin{center}
\textbf{Git}

Application that manages your projects and enables source control
\end{center}
\end{textblock}

\begin{textblock}{5}(9, 4)
\begin{center}
\textbf{GitLab}

Cloud host for your Git Repositories and a collaboration platform to work on projects
\end{center}
\end{textblock}

## Git vs. GitLab

\begin{textblock}{5}(2, 4)
\begin{center}
\textbf{Git}

Application that manages your projects and enables source control

\vspace{1cm}

\includegraphics[width=2cm]{../../assets/img/harddrive.png}
\end{center}
\end{textblock}

\begin{textblock}{5}(9, 4)
\begin{center}
\textbf{GitLab}

Cloud host for your Git Repositories and a collaboration platform to work on projects

\vspace{0.5cm}

\includegraphics[width=2cm]{../../assets/img/cloud.png}
\end{center}
\end{textblock}

## GitHub vs GitLab

## GitHub vs GitLab

\begin{textblock}{5}(2, 4)
\begin{center}
\textbf{GitHub}

Largest Git Repository Platform for open source projects. It is hosted by Microsoft.

\end{center}
\end{textblock}

## GitHub vs GitLab

\begin{textblock}{5}(2, 4)
\begin{center}
\textbf{GitHub}

Largest Git Repository Platform for open source projects. It is hosted by Microsoft.

\end{center}
\end{textblock}

\begin{textblock}{5}(9, 4)
\begin{center}
\textbf{GitLab}

FOSS Alternative to GitHub which is Self-Hosted.
\end{center}
\end{textblock}

## GitHub vs GitLab

\begin{textblock}{5}(2, 4)
\begin{center}
\textbf{GitHub}

Largest Git Repository Platform for open source projects. It is hosted by Microsoft.

\end{center}
\end{textblock}

\begin{textblock}{5}(9, 4)
\begin{center}
\textbf{GitLab}

FOSS Alternative to GitHub which is Self-Hosted.
\end{center}
\end{textblock}

\begin{center}
\vspace{1cm}

\Huge{=}
\end{center}

# Practical Usage

##

\begin{center}
\textbf{Practical Examples}
\end{center}

## What we will do

- Create a git repository
- Learn basic git commands
- Add some files to git
- Visit an old commit

## Creating a git repository

## Creating a git repository

- Go to GitLab Main Page (Projects Tab)

- Create a new project (Create a blank project)

- Give it a name. Select for the namespace "your account"

- You choose the visibility (since it is a testing project you can keep it to private)

- Click on create project

## Creating a git repository

\begin{center}
\includegraphics[width=0.7\textwidth]{../../assets/img/gitlab_create_project.png}
\end{center}

## Creating a git repository

Copy the \textbf{ssh} link

from the \textbf{code dropdown}

\begin{textblock}{8}(7, 3.5)
\includegraphics[width=8cm]{../../assets/img/gitlab_clone_ssh.png}
\end{textblock}

## Creating a git repository

- Open Git Bash in an directory where you want to put the project folder into

\begin{textblock}{10}(7, 3)
\includegraphics[width=0.7\textwidth]{../../assets/img/command_clone.png}
\end{textblock}

- The first time you do this, it will ask for a "Fingerprint" verification. You can just write "y" (for yes) and move on.

## Creating a git repository

- Use \textbf{cd} to get into the project directory (it is called how you called it in gitlab)

\begin{textblock}{5}(9, 1)
\includegraphics[width=0.7\textwidth]{../../assets/img/command_cd_repo.png}
\end{textblock}

## Creating a git repository

MacOS:

\begin{center}
\includegraphics[width=0.5\textwidth]{../../assets/img/command_ds_store.png}
\end{center}

## Creating a git repository

\begin{center}
You have created your first git repository
\end{center}

## Basic Git Commands

## Basic Git Commands

Most important one:

\begin{center}
\includegraphics[width=0.3\textwidth]{../../assets/img/command_git_status.png}
\end{center}

## Basic Git Commands

Most important one:

\begin{center}
\includegraphics[width=0.5\textwidth]{../../assets/img/command_output_git_status_1.png}
\end{center}

## Basic Git Commands

Viewing the Git History:

Just press \textbf{q} to exit the command. (If it hasn't already exited)

\begin{center}
\includegraphics[width=0.3\textwidth]{../../assets/img/command_git_log.png}
\end{center}

## Basic Git Commands

Viewing the Git History:

Just press \textbf{q} to exit the command. (If it hasn't already exited)

\begin{center}
\includegraphics[width=0.6\textwidth]{../../assets/img/command_output_git_log.png}
\end{center}

## Adding Files

## Adding Files

Go into the repository (can be with your normal file explorer) and change something in there.

Example Changes:

- Create a file
- Change something in a file
- Delete a file

\textbf{Warning:} Adding an empty directory is not enough.

## Adding Files

Rerun git status and see what happens

\begin{center}
\includegraphics[width=0.3\textwidth]{../../assets/img/command_git_status.png}
\end{center}

## Adding Files

Rerun git status and see what happens

\begin{center}
\includegraphics[width=0.7\textwidth]{../../assets/img/command_output_git_status_2.png}
\end{center}

## Adding Files

\textbf{Staging Phase} (Selecting your changes):

\begin{center}
\includegraphics[width=0.6\textwidth]{../../assets/img/command_git_add.png}
\end{center{}}

## Creating a Commit

Create the actual commit:

\begin{center}
\includegraphics[width=0.4\textwidth]{../../assets/img/command_git_commit.png}
\end{center}

## Nano

\begin{center}
\includegraphics[width=0.7\textwidth]{../../assets/img/nano.png}

Simply type the commit message

Save: CTRL + O

Exit/Submit: CTRL + X
\end{center}

## Creating a Commit

Rerun git log and see your new commit

\begin{center}
\includegraphics[width=0.7\textwidth]{../../assets/img/command_output_git_log_2.png}
\end{center}

##

\begin{center}
Congratulations you have created your first commit
\end{center}

##

\begin{center}
But observe that your changes are \textbf{not yet} on \textbf{GitLab}
\end{center}

##

\begin{center}
But observe that your changes are \textbf{not yet} on \textbf{GitLab}

Why is that?
\end{center}

## Synchronizing the Git Repository

Reasons:

- Local working
- Isolated Working

## Synchronizing the Git Repository

- Changes are only local. Not on gitlab
- We need to push the changes

## Synchronizing the Git Repository

Run the command

\begin{center}
\includegraphics[width=0.3\textwidth]{../../assets/img/command_push.png}
\end{center}

## Synchronizing the Git Repository

We can now verify in gitlab that the changes are indeed up there

\begin{center}
\includegraphics[width=0.7\textwidth]{../../assets/img/gitlab_changes_up.png}
\end{center}

## Synchronizing the Git Repository

Case

- Someone else made some changes
- You made changes on another device

## Synchronizing the Git Repository

Run the command to update your local repository

\begin{center}
\includegraphics[width=0.3\textwidth]{../../assets/img/command_pull.png}
\end{center}

## Synchronizing the Git Repository

\begin{center}
You can successfully collaborate with other people on your git project
\end{center}

##

\begin{center}
You see your data is now secured through Git and GitLab.
\end{center}

## Main Git Loop

1. Edit files with your normal software
2. Stage all the files
3. Commit all the files
4. Push to GitLab

##

\begin{center}
Now, try it for yourself
\end{center}

## Visiting old commits

To visit old commits run this command with the first few digits of the commit hash

Observe that the change you made is \textbf{not there anymore}

\vspace{5cm}

\begin{textblock}{4}(2, 5)
\includegraphics[width=\textwidth]{../../assets/img/command_checkout_commit.png}
\end{textblock}

\begin{textblock}{8}(6, 5)
\includegraphics[width=\textwidth]{../../assets/img/command_output_git_log_2.png}
\end{textblock}

## Going back to the main version

Observe that the change you made is \textbf{back}

\begin{center}
\includegraphics[width=0.3\textwidth]{../../assets/img/command_checkout_main.png}
\end{center}

## Last practice round

\begin{center}
Now try it for your self
\end{center}

## Congratulations

You can now

- Create and use a git Repository
- You can save files with Git
- You can upload and share projects with others
- You can recover old versions

# Extras

## Time for Feedback

\begin{center}
Please go to \textbf{https://feedback.thealternative.ch} and give me some feedback
\end{center}

## Image Sources

- _[Git Logo](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2F1000logos.net%2Fwp-content%2Fuploads%2F2020%2F08%2FGit-Logo.png&f=1&nofb=1&ipt=87afc05851e67721fbdb3385f4d30759cef2c01a9259fc2e0c7e412514a39b17&ipo=images)_
- _[Icon Library](https://libraries.excalidraw.com/?target=_excalidraw&referrer=https%3A%2F%2Fexcalidraw.com%2F&useHash=true&token=-oHt94MqUYHwJukCepy-x&theme=light&version=2&sort=default)_
- _[Image of Linus Torvalds](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fcdn.britannica.com%2F99%2F124299-050-4B4D509F%2FLinus-Torvalds-2012.jpg&f=1&nofb=1&ipt=084f6db63d522ffd96dc18812179d66320fa3d8ef8294f7e0fc32c757e0a9587&ipo=images)_
- _[HTML, CSS, JS Logo](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ficon-library.com%2Fimages%2Fhtml5-icon-png%2Fhtml5-icon-png-1.jpg&f=1&nofb=1&ipt=5a62f9cbf57f96c91711dc48ef2d57b16ab00cca7e1d4d8c15a122867f418640&ipo=images)_
- _[Box Icon](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ficon-library.com%2Fimages%2Fbox-png-icon%2Fbox-png-icon-2.jpg&f=1&nofb=1&ipt=dfed2dcb58e36c3091f38007ba2221d1f6cdf87d20fef05a816d5af13a0cfd3a&ipo=images)_
- _[GitLab Logo](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fcdn.freebiesupply.com%2Flogos%2Flarge%2F2x%2Fgitlab-logo-png-transparent.png&f=1&nofb=1&ipt=1c82a558a7078e51bedf72642990a5e8f28edc20f1995a4c4178abf1f3cf9b21&ipo=images)_
- _[Harddrive Icon](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fcdn0.iconfinder.com%2Fdata%2Ficons%2Ftechnology-73%2F66%2F1-1024.png&f=1&nofb=1&ipt=58388389fcfecf981aaed72085676aac65d545c53ce72eb3d94426e9190db7d6&ipo=images)_
- _[Cloud Icon](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fluminfire.com%2Fwp-content%2Fuploads%2F2018%2F08%2FCloud-Storage-Icon.png&f=1&nofb=1&ipt=90ecb680b4c8fb7829471f95fa56046cd19d3596dadcc1c7efebded8243ac56f&ipo=images)_
- _[Git Kraken Screenshot](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.gitkraken.com%2Fwp-content%2Fuploads%2F2021%2F03%2Fgk-product-2-1024x624.png&f=1&nofb=1&ipt=a3695791b8f56d97c7dfb4e2c2e281170a8751bce85e87671737d859723a0bf6&ipo=images)_
- _[Git Bash](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fseeklogo.com%2Fimages%2FG%2Fgit-bash-logo-B6475E8359-seeklogo.com.png&f=1&nofb=1&ipt=ea4993ce01121efe19bac022099f837bee6a01a9fc8f898b3ca32a687ac66d94&ipo=images)_
