# Git and GitLab Installation and Setup Guide

This a brief installation guide on how to install and setup git and connect it to your GitLab instance.

## Installation: Git

The instructions to install git differ from operating system to operating system

### Windows

Go to the Website **https://git-scm.com/downloads** and download the setup file for git and install git.

You can test if the installation worked by simply searching for a program called ``git bash'' on your system and running it.

### MacOS

You first need to install the MacOS package manager ``Homebrew''. If you have Homebrew already installed on your system you can skip this step.

To install Homebrew you need to open the program called ``Terminal'' and run the command

```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

Next we can install Git with Homebrew by typing the command

```bash
brew install git
```

You can test if the installation was successful by typing

```bash
git
```

into the terminal.

### Linux

You want to use your local package manager to install the `git` package.

**Ubuntu**

```bash
sudo apt install -y git
```

**Fedora**

```bash
sudo dnf install git
```

**Arch**

```bash
sudo pacman -S git
```

You can test if the installation was successful by typing

```bash
git
```

into the terminal.

## Setup: Git

Run the configuration commands in the ``Terminal/Git Bash''.

```bash
git config --global core.editor "nano"
git config --global user.email "your email"
git config --global user.name "your account name"
git config --global pull.rebase false
```

## Setup GitLab

1. Log into your GitLab

This is probably at the URLs

- \textbf{https://gitlab.uzh.ch}
- \textbf{https://gitlab.ethz.ch}

2. Generate an SSH Key with the following commands in the ``Terminal/Git Bash'' (if you already know that you have created an ssh key before you can skip this step)

```bash
ssh-keygen -t ed25519
# 1. For the first question just leave
#    it empty and press enter

# 2. For the second question enter a passphrase
#    for the key

# Sometimes you need to execute this command.
# Try and if it fails ignore it.
ssh-add ~/.ssh/id_ed25519
```

3. Get the public key by running in the ``Terminal/Git Bash''.

```bash
# Get the public key
cat .ssh/id_ed25519.pub
```

Select the output of the command with your cursor and press `CTRL + SHIFT + C`. This will copy the key into your clipboard.

4. Authorize the public key in GitHub. To do that go on GitLab and go under \textbf{Your Profile > Preferences > SSH Keys}. Click on ``Add Key'' and paste the key there. All other fields will fill out automatically. (Note: There is an expiration date field. By default, in one year the key will expire. When this happens you will get an e-mail notification and you need to redo steps 3 and 4).
