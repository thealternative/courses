echo "Building all Git Courses"
cd cheat_sheet
echo "Building Cheat Sheet: $(pwd)"
make
cd ..

cd install_guide
echo "Building Install Guide: $(pwd)"
make
cd ..

cd eth

cd crash_course
echo "Building ETH Crash Course: $(pwd)"
make
cd exercise_sheet
echo "Building ETH Crash Course Exercise Sheet: $(pwd)"
make
pdfunite crash_course_exercise_sheet.pdf crash_course_exercise_sheet_sol.pdf crash_course_exercise_sheet_complete.pdf
cd ..
cd ..

cd beginner_course
echo "Building ETH Beginner Course: $(pwd)"
make
cd exercise_sheet
echo "Building ETH Beginner Course Exercise Sheet: $(pwd)"
make
pdfunite beginner_course_exercise_sheet.pdf beginner_course_exercise_sheet_sol.pdf beginner_course_exercise_sheet_complete.pdf
cd ..
cd ..

cd ..

cd uzh
echo "Building UZH Course: $(pwd)"
make
cd exercise_sheet
echo "Building UZH Course Exercise Sheet: $(pwd)"
make
pdfunite git_and_gitlab_exercise_sheet.pdf git_and_gitlab_exercise_sheet_sol.pdf git_and_gitlab_exercise_sheet_complete_exercise_sheet.pdf
cd ..
cd ..
