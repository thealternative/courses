---
author:
- Noah Marti
title: Console Toolkit and Linux Toolbox
---

# Console Basics

## Basics

### What is a console

* “Keyboard + Text” interface to your computer
* Related Terms
	* Terminal (Emulator): Synonym to console (today)
	* Shell: SW running in terminal, processing I/O
	* Bash, ZSH: Instances of shells
	* Command Line (Interface (CLI)): Text-only input
	* Command Prompt

### What is a console

\includegraphics[height=0.8\textheight]{img/konsole.png}

### What is a console

* Displays different information
	* Username, hostname, current working directory, git status, exit code, time, etc.
* Can be customized

### Why a console?

* Most direct interface to your computer
* Great for advanced/complex tasks
* Can easily be automated
	* I.e. batch work
* Similar on all Unix systems
* The way to interact with other systems
	* Remote servers
	* Containers
* Last thing alive when you break your system

### Bash and other shells

* It is an interface between you and your computer
* It allows you to access the operating system’s services (i.e. run programs)
* *It is not designed as a programming language*, but can be used as such -> bash scripting
* other shells
	* sh, ksh, zsh, fish, dash...
	* Most commands work in any shell, but *some don’t*
	* Usually this doesn’t matter too much

### Moving Around

* pwd: print working directory
* ls: list
* mkdir: make directory
* cd: change directory
* mv: move
* cp: copy
* rmdir: remove directory
* touch: "create" a file
* cat: concatenate

### Pipes and Redirects

* Pipe: |
* Redirects: `>`, `>>`, `<`, `<<`
* Pipes connect programs together
* Redirects redirect streams (mostly used for files)

### Shell scripting

* *See our bash workshop*
* Write a set of commands to a file and execute the file instead of several commands
* Used for:
	* Automatisation
	* Repetitive tasks
	* Startup scripts

## File System

### File system

\includegraphics[height=0.8\textheight]{img/filesystem.png}

### Special Folders

* Your home directory: ~
* Parent folder: ..
* Current folder: .

### Hidden Files

* Start with a dot
* E.g. ".config"
* Can be used for files and folders
* `ls -a`

## Commands

### Commands and Arguments

Command

```
ls
```

Flags (options)

```
ls -l
```

Arguments

```
ls ./some/directory
```

## Navigation

### Clear terminal

* Type: `clear`
* Ctrl + l

### Tab completion

* Complete by pressing "tab"
* Can complete
	* commands
	* options
	* arguments

### Bash history

* Ctrl + r for reverse search
* .bash_history
* commands starting with a space are not added to the bash history

## Variables

### Environment variables

A few examples:

* $SHELL
* $BROWSER
* $PATH

You can also set your own variables

### Path variable

* $PATH
* Tells the system where executables are found
* Can be appended -> .bashrc

## Bash config

### Bashrc

* found in your home directory
* ```.bashrc```

### Aliases

* alias vscode="flatpak run com.vscodium.codium"
* .bashrc
* .bash_aliases

```
source .bashrc
```

## Getting Help

### Man pages

```
man [command]
```

### Help flag

```
[command] --help
```

### Tutorials

* vimtutor
* man cmus-tutorial

# RegEx

### Globbing

* Pathname expansion (or globbing)\
	 `files/qui*` becomes `files/quicknotes files/quiz`
* asterisk replaces any number of any character
* question mark replaces a single instance of any character

### find

* A general purpose tool for finding various files according to search filters.
* Especially nice if used with the -exec option or regular expressions:
* Remove all files (-type f) that end in .php (*.php):

```
find . -type f -name "*.php" -exec rm {} \;
```

* Display all files that contain a comment that contains TODO:

```
find ~/Documents -type f -exec grep -l "//.*TODO" {} \;
```

### grep

* A command to look for regular expressions (commonly just "regex").
* Think of regexes as being text patterns on steroids:

```
grep -P '^[0-9]' data.csv prints all the lines starting with a number
```

* We can put multiple greps together to chain filters!

*Hot tip:* Put alias grep='grep -P' into ~/.bashrc.

```
-P switches to "Perl regular expressions".
```

* Unfortunately, there are lots of regex standards with minor differences.
* Stick with one and don't worry about it (until you do).

### sed

* The stream editor
* This tool can search for regular expressions like we did with grep and perform various operations on the matched lines.

Example: Delete all comments from a file

```
sed -P 's/^#.*//g' file.py
```

# File conversion

## File conversion

### ffmpeg

* audio
* video

### pandoc

Hierarchical text formats

* markdown
* html
* latex

### convert (ImageMagick)

* images

### pdflatex

* latex

# Network

## Network tools

### wget

* A general purpose download tool.
* Mirror a webpage:

```
wget --mirror --convert-links --page-requisites \
--no-parent -e robots=off https://thealternative.ch
```

* Download all pdf files:

```
site=https://people.inf.ethz.ch/suz/teaching/252-0210.html
wget --no-parent -r -l 1 -A .pdf $site
```

### curl

* curl  is  a tool for transferring data from or to a server
* It supports these protocols: 
	* DICT, FILE, FTP, FTPS, GOPHER, GOPHERS, HTTP, HTTPS, IMAP, IMAPS, LDAP, LDAPS,  MQTT,  POP3,  POP3S, RTMP,  RTMPS, RTSP, SCP, SFTP, SMB, SMBS, SMTP, SMTPS, TELNET or TFTP
* Features:
	* proxy support
	* user  authentication 
	* FTP  upload
	* HTTP post
	* SSL connections
	* cookies
	* many more...

### lynx

* A CLI Browser for your terminal
* Useful if you are on a remote server
* No graphical backend necessary

### ping and traceroute

* Ping: sends a request to a server
* Traceroute: Traces the path a network package takes to the destination

### nmap

* Network exploration tools
* Scans for hosts and ports

## Version control and Backups

### Git

No more:

* thesis.pdf
* thesis_old.pdf
* thesis_copy.pdf
* thesis_finalversion.pdf
* thesis_finalversion2.pdf

*Version Control* but can be used as backup

### Git

* Track changes to your code
* Comment your changes
* Easily revert back to older versions
* Avoid/manage conflicts when working in teams
* Manage release versions and development versions
* Work on different branches at the same time

*Visit our git course*

### Git

\includegraphics[height=0.8\textheight]{img/git_states.png}

### Git

\includegraphics[height=0.8\textheight]{img/git_flow.png}

### Borg

* multiple snapshots
* encryption
* differential backup
* compression
* easy off-site backups

*See last years presentation about backups*

### rsync

* file copying
* remote and local
* delta transfer

```
rsync [Source] [Destination]
```

Specify (remote) source/destination:

`user@host:path`

## Remote access

### ssh

* secure shell
* remote access
* remote destination musst be running ssh server
* ports musst be open on router
* *Pro Tip: Dont use Port 22* if you are exposed to the internet

### ssh config

* config file for ssh
* found in ```~/.ssh/config```

Example:

```
Host euler
	HostName euler.ethz.ch
	User martinoa

Host minecraftserver
	HostName 192.168.0.55
	User Steve
	Port 65000
```

### ssh keys

* authentication via private key
* generate via ```ssh-keygen```
* common flags:
	* -t [type]
	* -b [bits]
	* -C [comment]
	* -f [output file]
* passphrase
	* encrypts ssh key
	* more security

*Never share your private key*

### Copy ssh keys


Easily copy your *public* key to a remote machine

```
ssh-copy-id -p [port] [user]@[hostname]
```

### scp

* secure copying
* file copying via ssh protocol
* same security as ssh protocol

```
scp [Source] [Destination]
```

Specify (remote) source/destination:

`user@host:path`

### sftp

* \soft{secure file transfer program}
* \soft{similar to ftp but over encrypted ssh transport}
* \soft{ideal for sftp servers}


### scp, rsync, sftp

* all can copy files
* my choice:
	* scp for simple copy operations
	* rsync for more complicated operations

### tmux

* terminal multiplexer
* control multiple terminals (sessions) from a single screen
* detaching and reattaching

New session:

```
tmux new -s [sessionname]
```

Deattach with Ctrl+b d

List all tmux sessions:

```
tmux ls
```

Reattach session

```
tmux attach -t [sessionname]
```

### tmux

Other things tmux can do:

* multiple windows in same session
* window panes

If tmux is not available (e.g. euler) use *screen*

# File editing

## File editing

### Well known tui editors

Today:

* nano
* vim
* emacs

Others:

* Kakoune
* similar to vim: vi, neovim

### nano

* easiest text editor
* keyboard shortcuts shown at the bottom
* `^[key]` -> Ctrl + [key]
* `M-[key]` -> Meta(Alt) + [key]

### vim

* very keyboard driven
* three modes:
	* normal mode: navigating and commands
	* insert mode: inserting text
	* visual mode: selecting text
* commands begin with ":"

*Pro Tip:* learn vim with vimtutor

* save with :w
* exit with :q

### vim

\includegraphics[height=0.8\textheight]{img/vim_modes.png}

### vim

The best invention since sliced bread. Observe:

* Composable shortcuts and modal editing!
* Tree-based undo with timetravelling!
* Can edit files remotely via SSH!
* Recursive keyboard macros!
* Takes weeks to learn!
* 30 years old!

### emacs

* Features:
	* Yes
* Both TUI and GUI
* GNU Emacs is a version of Emacs, written by the author of the original (PDP-10) Emacs, Richard Stallman.
* Exists since 1976

### emacs

* Calendar
* Simple calculator
* Programmable calculator
* Searching a directory
* Encrypting and decrypting document
* Send and read e-mails
* Search files using grep
* Spell checking
* Running shell commands and compiling code
* Version control
* Compare and merge files
* Games

### emacs

* major and minor modes
* only one major mode at a time
* 0-n minor modes at a time
* major modes: how to handle a file
* minor modes: small modifications
* buffers can be used like windows



# System

## Packages

### Package Manager

* no need to download shady .exe files
* package manager handles download, installation, dependencies 
* package manager depends on your system
	* Debian/Ubuntu: apt
	* Fedora: dnf
	* Arch: pacman

### Package Manager: apt

apt ...

* search
* update: fetch new updates of package database
* upgrade: upgrade packages
* install
* remove
* list
* show: show details to a package (dependencies, installation size, the package source)

### Alternatives

* flatpak
	* containerization to some degree
	* system independent
* snap
	* containerization
	* developed by canonical (ubuntu)
* aur
	* arch user repository
	* community developed packages
* appimage
	* one application = one file
	* no sudo rights necessary

### Compile it yourself

* Follow the instructions
* *Recommendation:* install self-compiled packages to ```/usr/local```

## Users and Groups

### Users and Groups

* "normal" users: [your username]
* root: "admin"

* user information in: ```/etc/passwd```
	* [username]:[passwor]d:[UID]:[GID]:[comment]:[home]:[shell]
* password hashes in: ```/etc/shadow```
	* root only (for obvious reasons)

*Fun Fact:* root is privileged not due to its name, but due to its ID

### Users

* useradd
* usermod
* userdel
* passwd

### Groups

* groupadd
* groupmod
* groupdel

```
sudo usermod -a -G [groupname] [username]
sudo usermod -aG docker noah
```

*Visit our docker course!*

### chmod

* change file mode bits
* permissions:
	* read
	* write
	* execute

-> octal permissions

### chown

* change owner

```
chown [user]:[group] [file]
```

### Getting sudo rights

*use visudo* to prevent multiple user modifying the file at the same time

```
sudo visudo
```

Configuration style

```
[user] [hosts]=([groups]) [commands]
noah ALL=(ALL:ALL) ALL
```

*Pro Tip:* use `sudo !!` to execute the previous command with sudo rights

## Processes

### List Processes

list processes

```
ps
```

* common flags for ps
	* -a all users
	* -u show details, select b user ID
	* -x all processes

### Signals

* SIGKILL: forcefull termination
```
kill -9
```
* SIGTERM: terminate process
```
kill -15
```
* SIGQUIT (Ctrl+\\): terminate process, generates core dump
```
kill -QUIT
```
* SIGINT (Ctrl+c): interrupt requested by the user

### Terminal Processes

* Ctrl+c kills a command
* Ctrl+z pauses a command
* ```bg``` sends a process to the background
* ```fg``` pull process to the foreground

Use top (or htop) to show processes

# helpful things

### helpful commands

* fdisk
* du
* top
* ranger
* echo
* cmus
* dd

### good to know

* TTY

### which terminal to use

* whatever floats your goat
* check possible encoding
* images in a terminal?

# Wrapping up

### What now?

*Visit our other courses*

* git and gitlab
* bash
* docker
* cryptography: theory and practice

### Course material
* These slides: [TheAlternative](http://thealternative.ch)

    \soft{soon TM}

### Sources

* previous TheAlternative courses
* the internet
* various `man` pages
* "trust me bro!"

<!-- -->

* \soft{Theme by} Christian Horea, [CC BY](https://creativecommons.org/licenses/by/4.0/)
