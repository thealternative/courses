# The Console Toolkit

This repository contains slides and exercise material for the Linux console course held in autumn semester 2018.

# Course Description

Now that you have your Linux system up and running, it's time to make the best
out of it. Learn how to utilize the console and work towards mastering it in
this two-part course. We will start with the basics, and present you some
useful terminal applications designed to simplify your workflow. Furthermore,
you may directly solidify the abilities you learn by solving short exercises.

This way, you will feel comfortable with using the console in no time!

# Course Contents

The following is just a brainstormy list based on content of past courses that TheAlternative held. This course aims to cover most of this list. The list only serves as a guideline, the speaker can of course decide to go further in-depth (e.g. with more live demos) or leave out certain topics.

- why console
- course goals
- opening console
- commands & arguments
- arguments with spaces
- options
- manual
- cmd history (arrows, search)
- globs, wildcards
- ctrl-c
- file system navigation
- cd
- pwd
- .. (the directory location)
- tab completion
- ls
- fs layout (man hier)
- text editing
- less
- cat
- cp
- mv rm
- mkdir, rmdir
- search path
- users: sudo, su
- software installation
- creating boot sticks (dd, sync)
- disk space
- scripting
- ssh
