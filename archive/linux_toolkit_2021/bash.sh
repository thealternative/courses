#! /bin/bash

keypause=200
commandpause=3

while read line 
do 
	xdotool key --delay $keypause $line
	sleep $commandpause
od < $1
