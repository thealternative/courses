\input{commons/partitioning}

The OpenSUSE partitioning menu is very advanced and gives you a lot of information. The most important is the summary you see at the top. It tells you all the modifications it will do to your hard drive and highlights the potentially dangerous ones in red.

The first thing to do here is to change the file system type to \emph{ext4}. OpenSUSE uses \emph{btrfs} and \emph{xfs} as defaults. The former being a newer file system with many advanced features but not fully reliable and the latter an old but robust alternative to \emph{ext4}.

\begin{enumerate}
	\item Click on \textit{Edit proposal Settings}
	\item For the \textit{File System for Root Partition}, pick \emph{ext4}.
	\item If you don't want a separate home partition, untick \textit{Propose Separate Home Partition}. See \emph{\secref{sec:partplan}} for more details on this.
	\item If you want a separate home partition, select \emph{ext4} for the \textit{File System for Home Partition}, then click ok
\end{enumerate}

The summary on the top should now be noticeably shorter. Carefully read the entries. Pay attention that no existing partitions are deleted or formatted, as this will destroy data. Shrinking an existing partition is fine.

Compare the summary with your partition plan and make sure all the required partitions are created. If so, you can continue. If not, you can manually partition your disk using \textit{Create Partition Setup}.

\troubleshoot{
	\textbf{Mac users please read:}

	If you have a Mac, you will need to perform some additional setup here to make OpenSUSE work with the rEFInd bootloader you previously installed:

	\begin{enumerate}
		\item Click on \textit{Expert Partitioner}.
		\item On the left hand side, click on your main drive (usually sda).
		\item You now see a list of partitions. Select the one labelled as \textit{EFI System Partition} or similar.
		\item Click on \textit{Edit...}
		\item You should see a dropdown menu that is currently set to \textit{/boot/efi}. Click inside the menu so that you can change the text. You need to change the text using your keyboard because the option you need is not in the dropdown menu.
		\item Change the text to \textit{/boot}
		\item Now, click \textit{Finish}.
		\item In the partitioner, click \textit{Accept}.\\
			\note{The installer will display a warning at this point, saying that openSUSE won't be able to boot with this setup. You can safely ignore this warning because you have rEFInd installed, which can still boot openSUSE. When the warning appears, just click \textit{yes}.}\\
			\note{After the first warning, yet another warning appears, saying that you install on an existing partition without formatting it. This is also fine. OpenSUSE will put its boot files next to the boot files used by Mac OS X, which is okay. Click \textit{Yes}.}
	\end{enumerate}

	If you feel uncomfortable during this step, feel free to ask a helper.

	\warn{Do NOT perform these steps when you have a Windows PC!}
}

You can now go back to \emph{\subsecref{sec:inst}{subsec:inst-suse}} and follow the remaining instructions if everything looks fine.\\
\note{If something looks wrong, please read the text box below.}

\troubleshoot{\input{commons/partitions-delete}}

\subsubsection{Manually partitioning your disk}

Ideally, the automatic partitioning suits your needs and you don't need to do this. But if the automatic tool fails, you can try creating your partitions by hand. To do this, click on \textit{Create Partition Setup\dots}.

\begin{enumerate}
	\item Choose \textit{Custom Partitioning (for experts)} and click \textit{Next}.
	\item Click on \textit{Hard Disks} on the left. Expand that section by clicking on the small arrow next to it. Then, pick your main drive, usually \textit{sda}.
	\item You should see a list of all your current partitions. If you haven't already done that from within Windows or Mac OS X, you should now shrink your existing operating system partition (that should be the largest in size).
		\begin{enumerate}
			\item Select the partition in the list and click \textit{Resize...} at the bottom.
			\item Choose \textit{Custom Size} and enter a new size in the text field. A good measure is to use halve it.
			\item Click \textit{OK}.
		\end{enumerate}
	\item Now create all the partitions you need.\\
		\note{If you have MBR, you first create an \textit{Extended Partition} and make it as large as possible. This partition serves as a container for more, smaller partitions.}
		\begin{enumerate}
			\item Click \textit{Add...}
			\item Select your partition size according to \emph{\secref{sec:partplan}}. Click \textit{Next}.
			\item Select your partition's usage. \textit{Operating System} is for the root partition, \textit{Data and ISV applications} is for your home partition and \textit{Swap} is for your swap partition. Click \textit{Next}.
			\item Under \textit{Format Partition}, select the file system you need according to \emph{\secref{sec:partplan}}. Under \textit{Mount point}, choose the mount point you need. Click \textit{finish}.
		\end{enumerate}
	\item You can also edit the mount point and formatting options of existing partitions by selecting them and clicking on \textit{Edit\dots}.\\
		\note{This is sometimes required in order to designate an EFI boot partition.}
\end{enumerate}

You can now go back to \emph{\subsecref{sec:inst}{subsec:inst-suse}} and follow the remaining instructions if everything looks fine. If not, please ask for help.

