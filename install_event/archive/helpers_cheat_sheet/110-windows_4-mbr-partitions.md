\mysubsection*{4 Primary Partitions In MBR}
\label{subsec:win-4pparts}

MBR can have a maximum of 4 primary partitions, so if you already have this many,
you'll need to do something about it. 
To fix the problem do the following:

* Delete any primary partition not needed anymore. (On HP -> `HP_TOOLS`, on 
  Lenovo check which ones are not needed.).
* Resize any one partition you want the data from.
* Use the data freed from the resized partition and create a new partition of 
  type `Extended Partition`.
* Use that container for any future partitions.

