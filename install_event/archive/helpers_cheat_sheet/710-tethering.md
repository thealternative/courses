\mysubsection*{Phone Tethering}
\label{subsec:tricks-tethering}

In case you have no internet connection:

- Take out your phone (or someone else's phone, don't steal though!) and plug it into your PC via USB
- On your phone, go to your Network Settings and navigate to USB tethering and turn that on. The exact procedure is different on a case-by-case basis.
- Be sure to activate WLAN on your phone so you don't use your mobile data.

