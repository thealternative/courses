\mysubsection*{GRUB Configuration}
\label{subsec:boot-grubconf}

### Ubuntu

See `/etc/default/grub` to edit kernel parameters.
GRUB configuration can be regenerated with `update-grub`.

### OpenSUSE

Try to configure GRUB with YaST. (-> Boot Loader Settings).

\pagebreak
