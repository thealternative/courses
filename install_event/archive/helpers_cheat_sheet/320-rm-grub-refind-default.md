\mysubsection*{Exchange GRUB To rEFInd}
\label{subsec:boot-grubrefind}

Use `efibootmgr` to set the boot order.

-   `efibootmgr` or `efibootmgr -v` (for verbose output) gives you the current
    boot order. Relevant outputs are `BOOTXXXX` where `XXXX` are four numerical
    digits.
-   Set the boot order with `efibootmgr -o XXXX,YYYY,ZZZZ`
-   Delete the GRUB boot entry with `efibootmgr -b XXXX -B` where `XXXX` is the
    number of the GRUB boot entry.
-   You can remove duplicate entries with `efibootmgr -D` if cleanup is needed.


