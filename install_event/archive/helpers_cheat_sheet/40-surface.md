Surface Pro 4
=============

These little things need some special treatment to get the touchscreen
to work.

After the installation, add the following repository:

http://download.opensuse.org/repositories/home:/maxf:/LD/openSUSE\_Leap\_42.1/

From there, install kernel-default-ipts and kernel-firmware-ipts (for
intel precose touch support)

After that, you need to copy some hardware descriptor files from the
windows partition:

-   Descriptor.bin: A HID descriptor file provided by vendor. The HID
    Driver appends the panel’s HID descriptor to information.

-   iaPreciseTouchDescriptor.bin: A HID descriptor file. A bare minimum
    HID descriptor descripting the device. The HID Driver appends the
    panel’s HID descriptor to information.

-   KernelSKL.bin: Touch Vendor provided OpenCL kernel includes touch
    algorithms.

-   SFTConfig.bin: Touch Vendor provided configuration binary.

These files are located in:

%Windir%\\inf\\PreciseTouch\\

Kernel will look for these user space binaries in /itouch folder under
specific names.

-   /itouch/vendor\_kernel\_skl.bin

-   /itouch/integ\_sft\_cfg\_skl.bin

-   /itouch/vendor\_descriptor.bin

-   /itouch/integ\_descriptor.bin

So to be able to try different vendor kernels using softlinks can be
used as:

-   ln -sf /itouch/KernelSKL.bin /itouch/vendor\_kernel\_skl.bin

-   ln -sf /itouch/SFTConfig.bin /itouch/integ\_sft\_cfg\_skl.bin

-   ln -sf /itouch/Descriptor.bin /itouch/vendor\_descriptor.bin

-   ln -sf /itouch/iaPreciseTouchDescriptor.bin
    /itouch/integ\_descriptor.bin

For further reference, ask Max or Aline or see

https://github.com/ipts-linux-org/ipts-linux/wiki\#user-space-components
