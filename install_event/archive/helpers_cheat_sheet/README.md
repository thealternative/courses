# Install Guide Cheatsheet

This repository contains a collection of common problems and their solutions we face during our Install Events. This "cheatsheet" should be printed each Install Event and handed out to our helpers on demand (typically not all helpers need a sheet). The document is written in very shorthand form because the helpers are expected to have enough Linux knowledge to make use of the cheatsheet. In principle, it could also be handed out to course attendants, but it would likely be entirely useless to them.

The format is markdown; we use Pandoc to build a PDF. The buildscript is included in [build.sh](build.sh).
