Before installing
=================

Switch from RAID to AHCI
------------------------

Especially newer laptops (2018+) have options to switch between RAID and AHCI boot mode.

If the laptop is running in RAID mode and Linux does not recognize its drives, it will have to be switched to AHCI. Unfortunately, this also means Windows 
needs to be reconfigured, otherwise it won't boot anymore. Just google [windows 10 switch from raid to ahci](http://triplescomputers.com/blog/uncategorized/solution-switch-windows-10-from-raidide-to-ahci-operation/).

If the drive still is not visible, try to look for an entry called `Fastboot` and set it to off (or  how Dell calls it, `Thorough`).

Disable fast boot in Windows 8/8.1/10
-------------------------------------

-   Open the control panel (Win 10: the old-style control panel, not the
    newish one) and select the power options

-   Click on „Choose what the power buttons do“ on the left bar

-   By acquiring admin rights click „Change settings that are currently
    unavailable“

-   scroll down and uncheck the box for fast startup

ThinkPad T.40 Serie BIOS Version

-   BIOS version should be greater or equal to 1.14. 2.x to be sure not
    to brick the system

How to detect 32bit EFI and how to deal with it
-----------------------------------------------

-   No provided install image will boot

-   Windows is installed in 32bit mode

-   Some older Macbooks have this config

-   To install a 64bit Linux (we never encountered a 32bit CPU with
    that), manually replace the bootloader with a grub standalone and
    boot the kernel manually. Reference\
    https://wiki.archlinux.org/index.php/Unified\_Extensible\_Firmware\_Interface\#Booting\_64-bit\_kernel\_on\_32-bit\_UEFI

Useful Macbook hardware bitwidth table
--------------------------------------

-   http://www.everymac.com/mac-answers/snow-leopard-mac-os-x-faq/mac-os-x-snow-leopard-64-bit-macs-64-bit-efi-boot-in-64-bit-mode.html

Getting to BIOS in Windows 8/8.1/10
-----------------------------------

-   Get to the power button (metro bar in 8/8.1, start menu in 10)

-   Press the shutdown button

-   Press and hold SHIFT while selecting Restart

-   In the appearing GUI select „Extended options“ or similar until „Go
    to UEFI firmware“

Find out boot type, disable hybrid boot mode: UEFI or BIOS?
-----------------------------------------------------------

-   MUST do this before booting the Linux live system, otherwise
    existing systems might be broken!

-   In the BIOS, go to the boot options and set either BIOS (often
    called legacy) XOR UEFI mode! **DO NOT USE HYBRID MODES.**

-   If available (e.g. on Lenovo Workstation laptops), disable CSM when
    using UEFI / enable it when using BIOS mode.

-   Save, reboot and check if the existing OS starts

    -   If yes, you found the right mode

    -   If no, it’s the other mode, so switch it and don’t forget to
        attempt to boot the existing OS again, this time it must work or
        there’s a real problem.

Find out the partition table type: EFI or MBR?
----------------------------------------------

-   MUST find this out before installing, can mess up the system
    otherwise!

-   Check for a partition labeled “EFI” and formatted as FAT32
    (generally pretty small). Note that this partition might not be
    visible from Windows.\
    If there is such a partition, it’s EFI, else it’s an MBR table.
    Remember that type!

-   At the last step of installing, double-check that the installer
    detected partition type correctly.


Turn off Bitlocker
------------------

On some laptops (including all Surface devices) BitLocker Drive Encryption is enabled by default. It needs to be disabled first.

**Do not change any BIOS/UEFI settings before disabling BitLocker!** You will have to provide the decryption key otherwise, which the user typically has no access to, which in turn makes the entire disk unreadable.

### Disabling BitLocker

To disable BitLocker, do the following:

* Launch a command prompt with administrator rights and use `manage-bde -off C:`, where `C:` is the drive you wish to decrypt.
* Use the command `manage-bde -status` to query the decryption progress.
* You will need to wait until decryption is complete, which can take up to an hour depending on the number of files on the drive. 

After decryption, continue installing Linux as usual.

### Unlock BitLocker Encrypted Devices

If you change any UEFI settings on a BitLocker encrypted device (typically Surface devices), you will be prompted for the BitLocker key on next boot.

Since Surface devices come encrypted out of the box, the user does typically not have that key and Windows will refuse to boot. If this happens, resetting the UEFI settings to factory settings should fix the issue.

Alternatively, you can just enter the correct Bitlocker key. This works only if the user has a Microsoft account linked to the device. You can get the BitLocker key as follows:

* On another device, google for "BitLocker Recovery Key". You should find several Microsoft Support or FAQ pages on how to recover the key.
* Search for a link saying "To get your recovery key, go to BitLocker Recovery Keys" or similar. Go there.
* Ask the user to sign in using their Microsoft account. The website will then display their recovery key, which can be used to unlock the device.

