Partitioning
============

Watch for hybrid HDD/SSD systems
--------------------------------

-   Intel provides an SSD cache (found on a HP laptop). It doesn’t even
    show under Windows. On OpenSuse, it show up as an extra hard disk
    labeled: Intel Fast Flash.

    DO NOT TOUCH this disk.

-   We experienced problems resizing the hard disk on that machine (it
    said it’s inconsistent). To solve this, you have to start up Windows
    and disable Intel Smart Response in the utility installed on
    Windows.

MBR system setup
----------------

-   make sure system has no EFI! See above: “Find out the partition
    table type: EFI or MBR?”

-   Each HDD can only have max. 4 partitions which are either primary or
    logical, so I recommend first creating a new logical partition which
    again can contain 4 partitions, which we can all use for the Linux
    installation

-   If there are already 4 primary partitions on the disk (e.g. old HP
    laptops), you must delete a partition. We typically choose the ones
    like „HP TOOLS“.

-   More on this topic can be found here:\
    http://wiki.project21.ch/Partitionierung\_von\_HDs\_mit\_vier\_primären\_Partitionen

EFI system setup
----------------

-   make sure the system has EFI! See above: “Find out the partition
    table type: EFI or MBR?”

-   Lenovo \*\*40 =&gt; BIOS update before proceeding!

<!-- -->

-   make sure, that the system has booted in EFI mode (check whether
    /sys/firmware/efi exists)

-   make sure the system found the efi partition and it is mounted at
    /boot/efi (type ’mount’ in console, which lists all mounted devices)

-   when selecting the partition scheme for installation, check whether
    the efi partition is recognized and will be mounted at /boot/efi

-   If any EFI bugs occur, check the wiki article for EFI, which
    contains workarounds for many bugs.

Recommended partitions on a Linux system:
-----------------------------------------

-   At least 20 GB for /

-   swap partition at least as large as total amount of installed RAM

-   optional own partitions for /home, /usr, /var, … whatever the
    clients wish. Note that this is a religious question without clear
    answer which option is better:

    -   Advantage of separate home: More clear to backup (e.g. with dd),
        more independent of system

    -   Advantage of just one partition („/“): The entire space can be
        used for whatever, no separate and fix space constraint for
        system and home.

