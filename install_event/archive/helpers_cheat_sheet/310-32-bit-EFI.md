\mysubsection*{Detecting 32bit EFI \& Dealing With It}
\label{subsec:boot-32bit}

-   No provided install image will boot
-   Windows is installed in 32bit mode
-   Some older Macbooks also have this configuration

It is possible to install Linux on such devices, but requires some more handwork. Note that we only ever found this configuration on 64 bit processors, so it's usually possible to install 64 bit Linux.

* Ask the Patrol for a 32-bit-Grub-USB.
* Plug both the 32-bit-Grub-USB and the Linux Installer USB in the device
* Boot from the 32-bit-Grub-USB
* You should land in a grub shell. The command prompt should say `grub>`, not `grub-rescue>`. If you see `rescue`, something went wrong.
* Type `ls`, you should see a list of partitions in the form `(HD0,GPT1)`. Try to guess which disk (HD0, HD1, HD2...) is the USB Installer - they usually have two GPT partitions. 
* Type `set root=(HDX,GPT1)`, X being the number of the disk you think is the installer.
* If you get some error, it was the wrong partition or disk. Reboot and try another.
* The Kernel files reside in `/casper/vmlinuz.efi` and `/casper/initrd.lz` for Ubuntu installers, `/boot/x86_64/loader/linux` and `/boot/x86_64/loader/initrd` for opensuse.
* Load the vmlinuz by typing: (change path as required)
        `vmlinuz casper/vmlinuz.efi`
* Load the initial ramdisk by typing: (change path as required)
        `initrd casper/initrd.lz`
* You can use tab completion in the above step. If there are multiple files starting with `vmlinuz` or `initrd`, you usually need the one with the shortest file name.
* If you can't find the kernel files, you probably loaded the wrong partition or disk. Reboot and try again.
* Type `boot`, hit enter and pray.
* If all goes well, the installer now boots. Install Linux as normal.

After the install, you might still need to install the 32 bit version of grub in order to boot anything at all. If Linux doesn't boot now, do the following:

* Plug in the 32-bit-Grub-USB, unplug all other USB keys.
* Boot the 32-bit-Grub-USB. Type 'ls'. Try to guess on which partition you installed Linux before (root partition)
* `set root=(HD0,GPTX)`, X being the partition number you installed Linux on. 
* The kernel files usually reside in `/boot/grub` or `/boot/distroname`. Just like before, load the vmlinuz.efi and initrd files.
* Type `boot`, hit enter and PRAY.
* If you're lucky, the freshly installed Linux now comes up.
* once it is up, `sudo grub-install --target=i386-efi /dev/sda1` (if your boot partition is not `sda1`, change accordingly).
* regenerate the grub config (`grub-mkconfig -o /boot/grub/grub.cfg`). Change the path accordingly.
* Test whether Linux now boots.


\cleartooddpage
