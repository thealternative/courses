# Flash Stick

You will prepare a stick from which you can install Linux from.

_If you are at one of our Install Events, we already prepared USB sticks. Choose a distribution (read on) then come to the welcome desk to get your stick._

## Choosing a distribution

In the words of Richard Stallman:

> What you’re referring to as Linux, is in fact, GNU/Linux, or as I’ve recently taken to calling it, GNU plus Linux. Linux is not an operating system unto itself, but rather another free component of a fully functioning GNU system > made useful by the GNU corelibs, shell utilities and vital system components comprising a full OS as defined by POSIX."

In simpler terms: Linux by itself is not a full operating system (rather than a vital technical part of it). You need to choose a distribution (a "wrapper" around Linux) which will define to a large extend how you as a user will interact with the operating system (like installing new programs, how the desktop looks by default, ...).

As choosing a distribution is heavily opiniated, we describe it here very short. Feel free to reach out to us for a personal recommendation based on your specific needs.

If you "just want Linux", we recommend you go with [Ubuntu](https://ubuntu.com/). As a more open but also very stable alternative we recommend [Fedora](https://getfedora.org/). Truly Free (as in Freedom!) is [FreeBSD](https://www.freebsd.org/).

For each of these distributions we give you starting points for a successful installation. If you want to install other distributions feel free to do so; and we will do our best to help you. If you are an enthusiast and want to try to install something more advanced (like [Arch Linux](https://www.archlinux.org/)) come join us :).

## Download ISO

You need to download an image containing the operating system installer. Visit the webpage of your favourite distribution and download it.

If you have to choose from multiple versions, here some guidance of commonly used terms:

- `amd64`, `i386`, ... refers to the architecture of your device. If you have a laptop / PC, it is very likely that you need `amd64` (else you'd probably know it).
- `LTS` stands for long term support; meaning this specific version of the distribution will receive security updates for much longer than other versions.  

## Create install USB Stick

Get a USB stick of at least 8 Gb (make sure you do not need the data on it anymore!).
Flash (="Put") the .iso file you have just downloaded on the USB stick.

On windows, you can use [rufus](https://rufus.ie/). On macOS, you can use [etcher](https://www.balena.io/etcher/).
