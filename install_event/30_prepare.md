# Prepare Windows / Mac

If you want to keep your existing operating system (Windows or macOS) you need to prepare it.
This will allow you to "double-boot", hence choose at startup which operating system to run.

If you do not want to keep your existing operating system, you can skip this section.

## About Partitioning

Disk partitioning is the separation of different regions on a hard drive or an SSD. 
This separation is used to install two different operating systems on a computer or 
simply to have different regions for different purposes (like a partition for your data and one for the programs).

We recommend to have at least 40GB of space partitioned for Linux. If you know you
need programs for your studies (e.g. Matlab, which is around 30GB), please have enough free disk
space to install the desired programs and probably some extra, just in case.

Be aware that repartitioning later is practically impossible, so try to be on the safe side.
If you do not have space constraints, with 100Gb all you wildest wishes should be fulfillable.

## Windows 10

If you have Windows (and want to keep it) follow on.

### Making Space for Linux

Shrink Windows partition:

- Open `create and format hard disk partitions` or `diskmgmt.msc`.
- Look for the large Windows partition at the bottom.
- Right-click on it and select `Shrink Volume...`
- Shrink it so you have enough space available.

If you are unable to shrink the partition, use a tool like `EaseUS Partition Master`
or try it in the Linux installer. Ask a helper for assistance.

If you do not have enough space, you can free up disk space with these tips:

- Open `Disk Cleanup` and click on `Clean up system files`. Select all that apply.
- Open `Add or Remove Programs` and remove unneeded programs
- Download `WinDirStat` from https://windirstat.net/ and find where the large files are hiding

### Disable Bitlocker

Bitlocker encrypts your disk, but unfortunately it is not compatible with Linux
boot loaders.

Disable Bitlocker:

- Open `cmd` as administrator: Press Windows Key, type `cmd`, then 
  right click on `Command Prompt` and select `Run as administrator`)
- Type `manage-bde -status` and look for `Conversion Status: Fully Decrypted`
- For any drive which is not `Fully Decrypted`, type `manage-bde -unlock C:` 
  (replace `C:` with the drive you need to decrypt), and press Enter. 
- Wait for decryption to end (execute `manage-bde -status` for status
  updates)

You can also try to disable Bitlocker over the UI (look for `Manage Bitlocker`)
but it has been confusing to do so in the last few years.

### Fast Boot

`Fast boot` allows Windows to boot faster, but when it is active, the windows 
file system will be inaccessible to other OSes (like Linux).

You can avoid the `Fast boot` mode by explicitly choosing to `Restart` (rather than `Shut down`)
in windows.

Alternatively, you can simply disable fast boot:

- Find the battery icon in the task bar
- Right-click and select `Power Options`
- Select `Choose what the power buttons do`
- Select `Change settings that are currently unavailable`
- Remove the check mark at `Turn on fast startup`

### Booting the installer 

Make sure your Linux install stick is plugged into your PC.

Click `Restart` in the Windows start menu, while holding down the `Shift` key.
A `Please wait`-text appears, after which Windows should enter the troubleshooting
menu.

From this menu you can choose a startup device, or enter the UEFI settings.

- To choose the startup device, select `Use a device`. As we want to boot from the
  Linux USB stick choose `USB FDD` or similar.
- To enter the UEFI settings, select `Troubleshoot`, `Advanced Options` and then `UEFI
  Firmware Settings`.

If none of this works, you can try to change the startup device immediately after booting.
This guide will explain how in the next chapter.

## macOS

<span style="color:red">
Note that on modern Apple computers it is generally not advisable to install Linux
(MacBooks after 2016 simply are not very compatible with Linux.)
</span>

Up-to-date information on the status of Linux support on MacBooks can be found
[here](https://github.com/Dunedan/mbp-2016-linux).

### Shrink your OS X partition

On OS X, we will resize your existing partition to make
space for your new Linux system.

- Go to `/Applications/Utilities` and open the `Disk Utility`.
- Select your main disk in the list to the left (usually the first or the largest).
- Select the tab `Partition`.
- On the left side there is a white rectangle with some blue threshold indicating the space consumed by Mac OS X.
- Click and hold the lower right corner of that rectangle and draw the cursor upwards, to shrink the volume.
- The text on the right will give you information about the freed memory.
- Once you are satisfied, click `Apply` to confirm the shrinking operation.

### Install rEFInd

We will install the bootloader on OS X before the Linux
installation. *rEFInd* will boot your USB Install Medium as well as your
operating system later on.

- Browse the web for [http://www.rodsbooks.com/refind/getting.html](http://www.rodsbooks.com/refind/getting.html) and scroll down a bit to click the link *A binary zip file*.
- Once the download has completed, go to the *Download folder* and extract the file.
- Open a Mac terminal by going to `Application/Utilities` and opening `Terminal`.
- Type `cd ` (with a space afterwards, but do **not** press *Enter*  yet) and drag the extracted folder into the terminal.
- Now, hit *Enter* and then type `./refind-install`.
- When prompted for your password, enter it. (You won't see what you enter at the password prompt at all. You have to enter your password blindly.)

### Troubleshoot

In case you get an error message saying *ALERT: SIP ENABLED*, you need to do the following:

- Turn off your Mac and start it again, pressing `Cmd + R` immediately after turning it on again (you might want to hold it for a while).
- Your Mac should boot in `recovery mode`. Go to `Utilities` and open up a terminal.
- Now type: `csrutil disable`.
- Then reboot back into your normal system and try to install *rEFInd* again.
- After installation, feel free to go once more into recovery and type `csrutil enable`.

### Booting the installer

We will now boot from your USB Install Medium.

- Plug your USB Install Medium into your laptop.
- Reboot your machine and *rEFInd* should see the USB key.
- Your USB installer may have several ways to boot. They all will show up and have a little USB icon to the bottom right.
- Usually the first USB installer is correct. If it doesn't work, reboot and try the others.
- If your USB installer doesn't show up, hit `Escape` to refresh the list. It should show up now.
- If you see multiple USB keys, try each of them until you find the Linux installer.
