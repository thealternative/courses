# Piracy

Pirates come in many forms. Some violently board ships, killing the crew and taking its possessions. Others simply want to read "Analysis I - Third Edition". This lecture will be about the latter.

We will explore different ways how to find what you want in the internet, and argue for its ethical and legal standing.


