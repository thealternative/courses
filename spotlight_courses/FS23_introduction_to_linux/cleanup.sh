#!/usr/bin/env bash

rm -rf *.aux *.bbl *.blg *.fdb_latexmk *.fls *.log _minted-slides *.nav *.out *.pgf pythontex-files-* *.pytxcode *.snm *.toc *.vrb
