# reset the system
rm -rf /var/db/repos/chymeric/
emerge -C samri
eix-sync

# reset the overlay repo
cd overlay/location/with/Chymera/account
git reset --hard commitwhichwasbeforethepull
git push -f origin master
