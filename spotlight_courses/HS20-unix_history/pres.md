---
author:
- Nicolas König
title: A Short History of Unix

---

# What is Unix?

## What is Unix?

### 

\bigtext{What is Unix?}

### The Definition

A system can call itself *Unix* if it conforms to the *Single Unix Specification* maintained by *The Open Group*.

<!--
  Talk about standardization later
-->

### Difference between Unix and Linux

Unix is an interface description, Linux is an OS

- Linux's syscall interface doesn't provide all the required functions
- Also, to be complient, many more things have to be provided
  - header files
  - struct definitions
  - even shell & command line utilities 
- More on that later

### Current Unixs

- MacOS
- AIX
- Only one Linux distributions (EulerOS)!
  - All of them still adhere to the standards very closely

<!-- 
  Doing the conformance tests is expansive
-->

# The Early Days

## The Early Days

###

\bigtext{The Early Days}

## Context for the creation

### Bell Labs

* Part of telephone-monopoly company AT&T
* very "free" research culture
  * The choice of research topic was largely decided by the researchers
* 9 Nobel price winners!


### Multics

* "MULTiplexed Information and Computing Service"
* Collaboration between Bell Labs, General Electric and the MIT
* Successor to MIT's Compatible Time Sharing System (CTSS)


<!--
  With modern eyes: Very weird features
  Grand unified memory, no distinction between files and "normal" memory
  both called resounding success and an utter failure
-->

### Thompson, Kerningham, Ritchi

```{=latex}
\begin{figure}
  \centering
  \includegraphics[width=0.5\textwidth]{img/kt_and_dr.jpg}
\end{figure}
```

### Thompson, Kerningham, Ritchi

```{=latex}
\begin{figure}
  \centering
  \includegraphics[width=0.6\textwidth]{img/bk.jpg}
\end{figure}
```

<!--
  Kerningham & Ritchi both worked on Mutlics
-->

### The PDP-7 

* 8K 18-bit words memory
* ca. 570 kHz
* Produced by the Digital Equipment Cooperation (DEC)

### The PDP-7 

\begin{figure}
  \includegraphics[width=0.5\linewidth]{img/pdp7-oslo.jpg}
  \caption{A restored PDP-7}
\end{figure}

<!--
  Weak computer
  No interest in buying a new one after Multics disaster
  Used for Circuit desing, but most of the time free
-->

## The First Unix

### A Game

\centering
First, there was a Game



### An IO-Scheduler

\centering
Then, an IO-Scheduler

<!--
  Disk inside was too fast 
  Needed a program to put data on it
-->

### An Operating System

\bigtext{"At some Point I realized I was just three weeks away from an operating system."}

<!--
  Still missing 3 (exec,) editor, shell, assembler
  Wife away 3 weeks -> one week per programm
-->

### The Unix Philosophy

\bigtext{Make each program do one thing well}

### The Unix Philosophy

\centering
If you only have this little memory, you can't make your programs complex!

\centering
It turned out, that this is a good idea in general

## Released Unix Versions

### The First Edition

Released in Nov. 1971

* Supported the PDP-11
* relatively feature-rich
* already included a Fortran compiler

<!-- 
  Story on how they got the PDP-11
  	Patent applications!
  Name comes from the manual, development very fast
    hand-inserted patches when it was send out
	"Love, Ken"
-->

### Common Utilities

* A lot of commonly known utilities already included in the first release
  * `cd`, `chown`, `ls`, `cp`, `dc`, `ln`, `su`, `who`, ...

### The Third Edition

Released in Feb. 1973

* Included a C compiler
* Included first implementation of pipes

### The C Programming Language

* Heavily influenced by BCPL
* Designed as a system programming language
* The language still looked slightly different
  * The Dialect is known as K&R-C
* Publicised in the book "The C Programming Language" by Kerningham & Ritchi

### The C Programming Language

```c
foo(a, b)
  int a;
  int *b;
{
  return a + *b;
}
```
### Fourth Edition

Released in Nov. 1973

* The first edition almost completely written in C
  * Made portability possible
* Typically licensed with source code (and for a nominal fee)

### Seventh Edition

Released in Jan. 1979

* Many new tools still in use today
  * `awk`, `make`, `sed`, `tar`
* Included the first version of the Bourne Shell (`sh`)
* Included a fully featured Fortran77 Compiler

### A story of overwhelming success

The number of installed Systems rapidly increased

- 2. Edition (Jun. 1972): 10 systems inside AT&T
- 5. Edition (Jun. 1974): 50 systems inside AT&T

By 1977, it was running on 500 sites, including 125 Universities

<!--
  Mind how large systems where at that time!
-->

# The Unix War

## The Unix War

###

\bigtext{The Unix War}

## The two large competitors

### Berkley Software Distribution (BSD)

* Thompson was a visiting Professor at UCal in the year 1975/1976

There, many new tools and features where developed

  * the editor `vi`
  * `sendmail`
  * a Pascal Compiler

Then, with 3BSD (1979), a full Unix distribution was released

<!--
  Worth mentioning: 
  - the Berkley Fast File System
-->

### BSD

They also wrote a port to DEC's new VAX architecture

- Added support for Virtual memory

<!--
  C paid off!

  Might not be clear today, but DEC was __big__ back then.
  VAX and Unix where so far intertwined that "Not all the world's a VAX" made
  it into the "10 commandments for C programmers"
    
  The only other two ports existing was an intercal (7&8)/32 ports
-->

### 4.2BSD

Released in 1983

- Added sockets and a complete TCP/IP-stack

<!--
  THE defining feature for BSD-Linux
-->


### System V

In 1982, antitrust legislation forces AT&T to break up

- Lifts the ban on selling software

<!--
  Baby Bells!
-->

### System V

\centering
AT&T immideatly proceeded to bundle Unix for commercial use and released System V in 1983

<!--
  Not the first commercial AT&T Unix release, 1981 System III (a lot of the
  legal stuff had already been settled)
-->

### System V Release 1 (SVR1)

- Added an extensive interprocess communication API
- Also included many features from BSD
  - `curses`
  - `vi`

### Derivatives of the Two

There where many derivatives by different companies.

For BSD:

- SunOS
- Xenix

and for System V:

- HP-UX
- AIX

<!--
  Even Microsoft made their own!
-->

### Derivatives

```{=latex}
\begin{figure}
  \includegraphics[width=0.8\linewidth]{img/unix_tl.png}
\end{figure}
```

### System V Release 4 (SVR4)

- The most successful release of System V
- Result of a unification drive between several Unix vendors
- Added a lot of features from all over the Unix world
  - `ksh`
  - ELF file format
  - TCP/IP-support

### X/Open

The drive for unification was formalized in the formation of the X/Open consortium

- It was supposed to provide compatibility guides and standards
- Would later go on to acquire the rights to the Unix name

BSD, out of fear over the influence of AT&T & Sun, founded its own alternative, the
Open Software Foundation.

* AT&T then founded, as an answer, Unix International.

<!--
  When I'm saying BSD, I mean people using BSD
  OSF != FSF, carefull
-->

### Differences

There were huge differences between these two distributions

Both technical

- System V `STREAM` (introduced with SRV3) vs. Berkley `socket`

and culturally

- "long-haired Berkley" vs. "short-haired AT&T"

<!--
  Example: 	AT&T: System V: Consider it Standard
  			BSD Leute 4.2 > V
  		   
-->

### Differences

\centering
The differences between the two are still found all throughout UNIX!

### Differences

From bigger

* System V used `/usr/bin` and `/usr/sbin`, Berkley `/bin` and `/sbin`

to smaller things

* The `ps` command takes two kinds of arguments
  * `ps aux`: BSD-style arguments
  * `ps -eL`: System V style

### 1-800-ITS-UNIX

Berkley System Distribution, inc. (BSDi) was formed 1991

- immediately sued by AT&T (1992)
  - Usage of the Unix trademark
  - Illegal distribution of AT&T source code
- Settled out of court in favor of BSDi (1994)

<!--
  Every story of the history of Unix should mention the law suits
  almost all AT&T code stripped in previous releases
  in the end, AT&T lost, only a few source files had to be removed and a few more given attribution
-->

<!--

# The Ascend of Linux

## The Early Days

### The GNU project

### Linus Torvalds

### The Email

-->

# Standards

## Standards

###

\bigtext{Standards}

## Early Standardisation

### The ANSI-C Standard

* drive for standardization started in the early 1980s
* Many new features added
  * `void`, function prototypes, type qualifiers,...

Finally released by ANSI as *X3.159-1989*, it usually called ANSI-C or C89

<!--
  Released in 1989, as the name says
-->

### Posix.1

* "Portable Operating System Interface" + -x for Unix
* standardizes the interface C programs use to interact with the OS
* can be implemented by any OS!
* Standadized by *ISO/IEC 9945-1:1990*

<!--
  I'll quickly skip over Posix.2, which defined a whole bunch of things about the
  execution eviroment such as the interface to the C compiler and shell utilitties
-->

### Posix.1b/c

There where two important extensions for the standard

- Posix.1b\
  realtime extensions
- Posix.1c \
  posix threads

Eventually leading to *ISO/IEC 9945-1:1996*, a revised version of *Posix.1*

<!--
  You will see these when looking through man pages!
  Interestingly, these real time extensions are different from SRV4s!
-->

### The Open Group

- Formed in 1996 from a merger between X/Open and the Open Software Foundation
- Inherited the rights to the Unix name from X/Open
- Continued X/Opens standarization

<!--
  Remmeber X/Open from before
-->

### X/Open Portability Guide

X/Open (and later The Open Group) released portability guides (XPG)

- based upon Posix and typical practices
- The guides where later renamed to "Single Unix Specification" (SUS)
- *XPG4* and *XPG5* (later *SUSv1* and *SUSv2*) where the two most influential standards
  - Conformance required to call something Unix!

<!--
  Remember X/Open from before?
  X/Open turned into The Open Group after the Open Software
-->

### Posix.1-2001/SUSv3

in 1999, The Open Group and the ISO/IEC Joint Technical Committee 1 collaborated on a new standard

- Meant to unify SUSv2 and Posix
- Eventually resulted in *Posix.1-2001* / *ISO/IEC 9945:2002* / *SUSv3*
  - most commonly referred to as *SUSv3*
- 3700 Pages!

### Posix.1-2001/SUSv3

Split in four part

- *Base Definition*\
  Concepts, Definitions, Contents of headers
- *System Interfaces*\
  Specification of various functions (1123 in total)
- *Shell and Utilities*\
  Specifies the shell and utilities (160)
- *Rational*\
  Contains explanations and justifications

### Posix.1-2008 / SUSv4

- The newest standard
- Not many changes relative to the previous standard

## Weird Features

### 

\centering
A few weird features of earlier distributions made it into the standards

### gets()

```c
char *gets(char *);
```

Reads a line from stdin.

- No way to tell gets the length of the buffer!

### hsearch()

```c
#include <search.h>

int hcreate(size_t nel);
// The two available actions are FIND and ENTER
ENTRY *hsearch(ENTRY item, ACTION action);
void hdestroy(void);
```
Does anyone notice something weird?

### insque()

```c
#include <search.h>

void insque(void *elem, void *prev);
void remque(void *elem);
```

These functions require you to pass in structs with a certain layout!

<!--
  They also do only have the job, the other have you have to do yourself
  Direct wrapper around `incque` instruction on VAX machines
-->

### Prefixed struct fields

```c
struct addrinfo {
               int              ai_flags;
               int              ai_family;
			   /* ... */
               struct addrinfo *ai_next;
           };
```

Why are the fields in these structs prefixed?

- K&R-C just threw struct fields in the global namespace!

### Shared Memory

There are two (incompatible) shared memory implementations! 

- System V-style: `shmget`
- Posix Style: `shm_open`

# Conclusions

## Conclusions

### Conclusions

\centering
We know know both what Unix is and where it comes from

###

\centering
Thank you for listening! Any questions?
