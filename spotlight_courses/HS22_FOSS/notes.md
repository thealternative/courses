# Notes FOSS talk

## Definition

Free meaning

- free as in free speech vs. free as in free beer
- often, the term "libre" is used to make this clear
- you can still earn money with free software

open source meaning

- this is what we call code
- this is readable by humans, provided they know the language
- you can kinda see what this does
- this is not readable for a computer

binary

- this is what the computer can execute
- text stuff is still visible
- very hard to reverse-engineer and see how it works

## history

early computing

- scientific hacking culture
- software was delivered alongside the machine
- the source code was delivered as well
- the scientists had to adapt software for their needs

proprietary software

- larger software
- started to only ship the binary
- two big events
- lawsuit was dropped --> bundled software is now okay
- patentable software --> software is very young --> 25 years is A LOT

fsf

- four freedoms
- based on moral points
- they are a little dogmatic
- they don't do compromises

osi

- sells open source as a convenience rather than a moral obligation
- they are less harsh on the defintion
- free software is always open source, but not vice versa

foss

- thus, we stick the two words together and call it foss
- people also call it "FLOSS" (libre), but this is kind of a silly word

gpl

- if software is free or open depends on the license
- software has per se a copyright protection
- with a license, you state how your software has to be used
- gpl based on four freedoms of fsf

bsd license

- this is a bit of a full slide, but this is the whole license text
- it basically says:
	- you can do with the software whatever you want
	- if this software is used for evil, the writer is not at fault

copyleft

- the main difference between the two is the copyleft (a joke from copyright)
- modified gpl source code has to be redistributed under gpl as well
- example: linux, many forked linux and added their own stuff, and linux could just take this code

performance

- linus' law
- it was discovered that it doesn't scale well with large numbers of people, but the principle still holds
- even if you are not a coder, someone else is
- if malicious stuff is found, one can fork the code and remove that

signal vs. whatsapp

    Subscriber name
    Payment information
    Associated IP addresses
    Email addresses
    History logs
    Browser cookie data
    Other information associated with two phone numbers
