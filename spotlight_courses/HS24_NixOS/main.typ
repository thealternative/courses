#import "@preview/touying:0.5.3": *
#import "../../typst_template/thealternative.typ": *
#import "@preview/diagraph:0.3.0": *

#show: thealt-theme.with(
  aspect-ratio: "16-9",
  config-info(
    title: [Spotlight 1: NixOS],
    subtitle: [Subtitle],
    author: [Ferdinand Pamberger, Valentin Kaas],
    date: datetime.today(),
    institution: [Institution],
  ),
)
#show: codly-init.with()
// #show link: underline

#codly(
  stroke: 2pt + black,
  // inset: 5pt,
  lang-inset: 0.5em,
  zebra-fill: none,
    // display-name: false,
  languages: (
    nix: (
      name: " Nix",
      icon: text(font: "Iosevka", "\u{f1105}"),
    ),
    bash: (
      name:  " Bash",
      icon: text(font: "Iosevka", "\u{ebca}")
    )
  )
)
#set quote(block:true)


#title-slide()

// FIXME: this TOC is only for us to have an overvview while writing the talk
// #outline(depth: 1)

== Today's topics
- What sets *`NixOS`* apart from other distros? 

- benefits of *`Nix`* and *`NixOS`*
- Understand *why* Nix is cool and why it might be for you
- configuring *`NixOS`*
- *toolbox* of commands / websites
- some *interesting projects* for NixOS

== Nix, Nixpkgs, and NixOS
#align(center)[
  #image("img/nix-holy-trinity.png", height: 1fr)
]
== What sets NixOS apart
    - Declarative configuration
      - Easier to configure your system(s)
      - Easier to change, manage and maintain the configuration
      - Easier to back up and share with people
    - Easy to deploy machines and their configuration
    - Out of the box Rollbacks
    // - Configuration options for many programs & services
    - Free of side effects - Actually uninstalls packages and their dependencies
    // - Easy to set up VMs
    // - People can test each other's configurations using `nix run` and `nix shell` by just having access to the source
== What sets Nix apart
- Immutable & reproducible results
// - Easy to cross and static compile
- Source-based (you can alter packages without forking anything)
- Single package manager to rule them all! (C, Python, Docker, NodeJS, etc)
- Great for development, easily switches between dev envs with direnv
- Easy to try out packages without installing using `nix shell` or `nix run`
- Easy to set up a binary cache
- Easy to set up remote building
  - Distribute your builds accross an unlimited number of machines, without any hassle

= Crash course
== The Nix language basics
#columns(2, [
- A piece of nix code is a _Nix expression_
- Evaluating that expression gives a _value_
- The content of a _Nix file_ is a _Nix expression_
```nix
> 1 + 1
2

> "This is Nix!"
"This is Nix!"
```
#colbreak()
*Names and values:*
- Assign names to values
```nix
> a = 1

> a
a

> b = true

> b
true
```
])
=== let ... in
assign values and use them in a single expression
```nix
> let 
a = 4;
in
 a + a;

8
```
=== Attribute sets:
- collection of name-value pairs
#columns(2, [
```nix
> {a = 1; b = "2"; c = 3.0;}
{
  a = 1;
  b = "2";
  c = 3;
}
```
#colbreak()
access values in an attribute set:
```nix
> let
a ={b = 2; c = "a";}
in 
toString a.b + a.c

"2a"
```
])
=== Nix is lazy!
```nix
> {a = {lazy = 5;}; b = "2"; c = 3.0;}
{
  a = { ... };
  b = "2";
  c = 3;
}```

=== Lists
#columns(2, [
```nix
> a = [1 2 3 4 5]

> a
[
  1
  2
  3
  4
  5  
]
```
#colbreak()
```nix
> a = [1 {b = 3;} "hi!" ]

> a
[
  1
  { ... }
  "hi!"
]
```
])
=== Functions
#columns(2, [
  
```nix
> greet = name: "I am " + name + "!"

> greet "tux"
"I am tux!"

> greet
<LAMBDA>

> add = a: b: a + b

add 2 3
5
> add 2
<LAMBDA>

> addtwo = add 2

addtwo 3
5
```
])
=== Functions, a bit more in depth:
#columns(2, [
  
Function which returns a function:
```nix
> add = a: b: a + b
> addthree = add 3
```
`addthree` is now:
```nix
b: 3 + b
```
`a` is being replaced by 3.
#colbreak()
They can take attribute sets too!
```nix
> let
f = {a, b, c ? 3, ...}: a + b + c;
in
f {a =1; b = 2; d = "Hi!"}
6
```
])
= Nix derivations
== A simple bash script
#columns(2, [

```bash
#!/usr/bin/env bash
curl -s http://httpbin.org/get \
| jq --raw-output .origin
```
- curl "http://httpbin.org/get"
- get the field *origin* from the resulting json table using jq
#colbreak()
```bash
  ./what-is-my-ip.sh
<some_ip_address>
```
])
== Look, a Nix derivation!
```nix
{pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/nixos-24.05.tar.gz") {}}:
pkgs.writeShellScriptBin "what-is-my-ip" ''
  ${pkgs.curl}/bin/curl \
  -s http://httpbin.org/get | \
  ${pkgs.jq}/bin/jq \
  --raw-output .origin
''
```

=== What is important here?
#local(highlights: (
  (line: 0, start: 2, end: 2, fill: yellow),
  (line: 1, start: 1, end: 6, fill: red),
  (line: 2, start: 6, end: 11, fill: red),
  (line: 4, start: 6, end: 10, fill: red),

))[
  
```nix
{pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/nixos-24.05.tar.gz") {}}:
pkgs.writeShellScriptBin "what-is-my-ip" ''
  ${pkgs.curl}/bin/curl \
  -s http://httpbin.org/get | \
  ${pkgs.jq}/bin/jq \
  --raw-output .origin
''
```
]

=== What's happening here?
```nix
{pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/nixos-24.05.tar.gz") {}}:
```


We define what our *`pkgs`* are. 
This can be: 
- supplied when building this derivation
- set to a specific *`nixpkgs`* release
- both?

=== What's happening here?
```nix
pkgs.writeShellScriptBin "what-is-my-ip" ''
# some stuff happens here
''
```
- Take the *`writeShellScriptBin`* package from *`pkgs`*
- create an executable shell script containing everything written in *`'' ... ''`*

=== What's happening here?
```nix
  ${pkgs.curl}/bin/curl \
  -s http://httpbin.org/get | \
  ${pkgs.jq}/bin/jq \
  --raw-output .origin
```
- *`${}`* is used for escaping nix code
- *`pkgs.curl`* evaluates to the path of the derivation containing *`curl`*
- *`pkgs.jq`* evaluates to the path of the derivation containing *`jq`*

What are derivations? See in a bit!

== Let's build it!
```bash
❯ nix-build what-is-my-ip.nix
/nix/store/5jf23xp5nmqw8vhrjpgvbimrvd086v6z-what-is-my-ip
```
=== What is this?
```bash
❯ tree /nix/store/5jf23xp5nmqw8vhrjpgvbimrvd086v6z-what-is-my-ip
/nix/store/5jf23xp5nmqw8vhrjpgvbimrvd086v6z-what-is-my-ip/
└── bin
    └── what-is-my-ip

❯ /nix/store/5jf23xp5nmqw8vhrjpgvbimrvd086v6z-what-is-my-ip/bin/what-is-my-ip 
<some_ip_address>
```

=== What's inside?
```bash
cat /nix/store/5jf23xp5nmqw8vhrjpgvbimrvd086v6z-what-is-my-ip/bin/what-is-my-ip
#!/nix/store/516kai7nl5dxr792c0nzq0jp8m4zvxpi-bash-5.2p32/bin/bash
/nix/store/18jb60qfm9x7m9sczb7jkqjwr6hfl6rs-curl-8.7.1-bin/bin/curl \
-s http://httpbin.org/get | \
/nix/store/7ic2z0yz5hxw7lzz6rxsrdfv72yfy9vv-jq-1.7.1-bin/bin/jq \
--raw-output .origin
```
===== Which dependencies does this use?
- Well, curl and jq obviously, but anything else?


#scale(80%)[
#raw-render(
  ```dot
digraph G {
  graph[nodesep = 0.1];
  node[height=0.01, width=0.01, margin = 0.04, fontsize = 10];
  edge[minlen=1, arrowsize = 0.5];
subgraph depglibc {
  "libpsl-0.21.5"                                                    [label = "libpsl-0.21.5", shape = box];
  "openssl-3.0.14"                                                   [label = "openssl-3.0.14", shape = box];
  "zlib-1.3.1"                                                       [label = "zlib-1.3.1", shape = box];
  "bash-5.2p32"                                                      [label = "bash-5.2p32", shape = box];
  "keyutils-1.6.3-lib"                                               [label = "keyutils-1.6.3-lib", shape = box];
  "brotli-1.1.0-lib"                                                 [label = "brotli-1.1.0-lib", shape = box];
  "nghttp2-1.61.0-lib"                                               [label = "nghttp2-1.61.0-lib", shape = box];
  "oniguruma-6.9.9-lib"                                              [label = "oniguruma-6.9.9-lib", shape = box];
  "jq-1.7.1-lib"                                                     [label = "jq-1.7.1-lib", shape = box];
  "jq-1.7.1-bin"                                                     [label = "jq-1.7.1-bin", shape = box];
  "libssh2-1.11.0"                                                   [label = "libssh2-1.11.0", shape = box];
  "zstd-1.5.6"                                                       [label = "zstd-1.5.6", shape = box];
  "gcc-13.2.0-lib"                                                   [label = "gcc-13.2.0-lib", shape = box];
  "curl-8.7.1-bin"                                                   [label = "curl-8.7.1-bin", shape = box];
}

subgraph direct {
  "jq-1.7.1"                                                         [label = "jq-1.7.1", shape = box];
  "curl-8.7.1"                                                       [label = "curl-8.7.1", shape = box];
}

"libidn2-2.3.7"                                                    [label = "libidn2-2.3.7", shape = box];
"gcc-13.2.0-libgcc"                                                [label = "gcc-13.2.0-libgcc", shape = box];
"glibc-2.39-52"                                                    [label = "glibc-2.39-52", shape = box];
"xgcc-13.2.0-libgcc"                                               [label = "xgcc-13.2.0-libgcc", shape = box];
"libkrb5-1.21.3"                                                   [label = "libkrb5-1.21.3", shape = box];
"jq-1.7.1-man"                                                     [label = "jq-1.7.1-man", shape = box];
"publicsuffix-list-0-unstable-2024-01-07"                          [label = "publicsuffix-list-0-unstable-2024-01-07", shape = box];
"jq-1.7.1-doc"                                                     [label = "jq-1.7.1-doc", shape = box];
"libunistring-1.1"                                                 [label = "libunistring-1.1", shape = box];
"what-is-my-ip"                                                    [label = "what-is-my-ip", shape = box];

"curl-8.7.1-bin"                          -> "what-is-my-ip"       [color = "black"];
"bash-5.2p32"                             -> "what-is-my-ip"       [color = "red"];
"jq-1.7.1-bin"                            -> "what-is-my-ip"       [color = "green"];
"openssl-3.0.14"                          -> "curl-8.7.1-bin"      [color = "blue"];
"curl-8.7.1"                              -> "curl-8.7.1-bin"      [color = "magenta"];
"glibc-2.39-52"                           -> "curl-8.7.1-bin"      [color = "burlywood"];
"zlib-1.3.1"                              -> "curl-8.7.1-bin"      [color = "black"];
"glibc-2.39-52"                           -> "openssl-3.0.14"      [color = "red"];
"libssh2-1.11.0"                          -> "curl-8.7.1"          [color = "green"];
"openssl-3.0.14"                          -> "curl-8.7.1"          [color = "blue"];
"zstd-1.5.6"                              -> "curl-8.7.1"          [color = "magenta"];
"libidn2-2.3.7"                           -> "curl-8.7.1"          [color = "burlywood"];
"glibc-2.39-52"                           -> "curl-8.7.1"          [color = "black"];
"libkrb5-1.21.3"                          -> "curl-8.7.1"          [color = "red"];
"brotli-1.1.0-lib"                        -> "curl-8.7.1"          [color = "green"];
"nghttp2-1.61.0-lib"                      -> "curl-8.7.1"          [color = "blue"];
"zlib-1.3.1"                              -> "curl-8.7.1"          [color = "magenta"];
"libpsl-0.21.5"                           -> "curl-8.7.1"          [color = "burlywood"];
"openssl-3.0.14"                          -> "libssh2-1.11.0"      [color = "black"];
"glibc-2.39-52"                           -> "libssh2-1.11.0"      [color = "red"];
"zlib-1.3.1"                              -> "libssh2-1.11.0"      [color = "green"];
"glibc-2.39-52"                           -> "bash-5.2p32"         [color = "blue"];
"glibc-2.39-52"                           -> "jq-1.7.1-bin"        [color = "magenta"];
"oniguruma-6.9.9-lib"                     -> "jq-1.7.1-bin"        [color = "burlywood"];
"jq-1.7.1"                                -> "jq-1.7.1-bin"        [color = "black"];
"jq-1.7.1-man"                            -> "jq-1.7.1-bin"        [color = "red"];
"jq-1.7.1-lib"                            -> "jq-1.7.1-bin"        [color = "green"];
"jq-1.7.1-doc"                            -> "jq-1.7.1-bin"        [color = "blue"];
"glibc-2.39-52"                           -> "zstd-1.5.6"          [color = "magenta"];
"gcc-13.2.0-lib"                          -> "zstd-1.5.6"          [color = "burlywood"];
"libunistring-1.1"                        -> "libidn2-2.3.7"       [color = "black"];
"xgcc-13.2.0-libgcc"                      -> "glibc-2.39-52"       [color = "red"];
"libidn2-2.3.7"                           -> "glibc-2.39-52"       [color = "green"];
"glibc-2.39-52"                           -> "oniguruma-6.9.9-lib" [color = "blue"];
"bash-5.2p32"                             -> "libkrb5-1.21.3"      [color = "magenta"];
"glibc-2.39-52"                           -> "libkrb5-1.21.3"      [color = "burlywood"];
"keyutils-1.6.3-lib"                      -> "libkrb5-1.21.3"      [color = "black"];
"glibc-2.39-52"                           -> "brotli-1.1.0-lib"    [color = "red"];
"glibc-2.39-52"                           -> "nghttp2-1.61.0-lib"  [color = "green"];
"glibc-2.39-52"                           -> "jq-1.7.1-lib"        [color = "blue"];
"oniguruma-6.9.9-lib"                     -> "jq-1.7.1-lib"        [color = "magenta"];
"glibc-2.39-52"                           -> "zlib-1.3.1"          [color = "burlywood"];
"glibc-2.39-52"                           -> "keyutils-1.6.3-lib"  [color = "black"];
"libidn2-2.3.7"                           -> "libpsl-0.21.5"       [color = "red"];
"glibc-2.39-52"                           -> "libpsl-0.21.5"       [color = "green"];
"publicsuffix-list-0-unstable-2024-01-07" -> "libpsl-0.21.5"       [color = "blue"];
"libunistring-1.1"                        -> "libpsl-0.21.5"       [color = "magenta"];
"glibc-2.39-52"                           -> "gcc-13.2.0-lib"      [color = "burlywood"];
"gcc-13.2.0-libgcc"                       -> "gcc-13.2.0-lib"      [color = "black"];
}
  ```,
  engine: "dot"
)]


=== So what exactly is a derivation, now?
#quote(attribution: link("https://wiki.nixos.org/wiki/Derivations")[NixOS Wiki])[

A Nix derivation is a specification for running an executable on precisely defined input files to repeatably produce output files at uniquely determined file system paths. Simply put, it describes a set of steps to take some input and produce some output in a deterministic manner.
]

Said simply: Take inputs, do something, get output. Same inputs, same outputs. 

== What can we do with our Package?

=== Create a developement Shell using it!
```nix
let
  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/nixos-24.05.tar.gz") {};
  what-is-my-ip = import ./what-is-my-ip.nix {inherit pkgs;};
in
  pkgs.mkShell {
    packages = [what-is-my-ip];
    shellHook = ''
      echo "Hello from TheAlt!"
    '';
  }
```
===
```bash
❯ nix-shell what-is-my-ip-shell.nix 
Hello from TheAlt!

[nix-shell:~/examples]$ what-is-my-ip
<some_ip_address>
[nix-shell:~/examples]$ which what-is-my-ip
/nix/store/5jf23xp5nmqw8vhrjpgvbimrvd086v6z-what-is-my-ip/bin/what-is-my-ip
```
Note: *exact same hash!*
=== Put it into a docker image!
```nix
let
  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/nixos-24.05.tar.gz") {};
  what-is-my-ip = import ./what-is-my-ip.nix {inherit pkgs;};
in
  pkgs.dockerTools.buildImage {
    name = "what-is-my-ip-docker";
    config = {
      Cmd = ["${what-is-my-ip}/bin/what-is-my-ip"];
    };
  }
```
=== Put it into a system's packages!
#text(size:10pt, [
#columns(2, [
```nix
let
  nixpkgs = fetchTarball "https://github.com/NixOS/nixpkgs/archive/nixos-24.05.tar.gz";
  pkgs = import nixpkgs {};
  what-is-my-ip = import ./what-is-my-ip.nix {inherit pkgs;};
  nixos = import "${nixpkgs}/nixos" {
    configuration = {
      users.users.alice = {
        isNormalUser = true;
        # enable sudo
        extraGroups = ["wheel"];
        packages = [
          what-is-my-ip
        ];
        initialPassword = "tux";
      };

      system.stateVersion = "24.05";
    };
  };
in
  nixos.vm
```
#text(size: 15pt,[
- Build the vm
  - (using nix-build what-is-my-ip-vm.nix)
- have what-is-my-ip installed!
])])
])

= NixOS
== What is NixOS?

#quote(attribution: link("https://nixos.org/manual/nixos/stable/#preface")[NixOS Manual])[
  A Linux distribution based on the purely functional package management system *Nix*, that is composed using modules and packages defined in the *Nixpkgs* project 
]

- *nixpkgs*: NixOS's package repository

  $==>$ gigantic collection of nix code


== A look at nixpkgs
- Giant mono-repo with build recipies (derivations) for 100 000+ programs
  - As of today, over 4 Million LOC!
- *nixpkgs* lib
  - Big collection of helper functions to make programming in nix more convenient
- *NixOS* itself too!

== `nixpkgs/pkgs/by-name/ty/typst/package.nix`

#columns(2,
[#text(size: 12pt,
```nix
rustPlatform.buildRustPackage rec {
  pname = "typst";
  version = "0.12.0";
  src = fetchFromGitHub {
    owner = "typst";
    repo = "typst";
    rev = "refs/tags/v${version}";
    hash =
"sha256-OfTMJ7ylVOJjL295W3Flj2upTiUQXmfkyDFSE1v8+a4=";
  };
  cargoLock = {
    lockFile = ./Cargo.lock;
    outputHashes = {
      "typst-dev-assets-0.12.0" =
 "sha256-YLxLuhpAUzktjyprZAhZ4GjcXEDUDdLtSzc5onzLuto=";
    };
  };
  nativeBuildInputs = [
    installShellFiles
    pkg-config
  ];
  /* omitted */
  meta = {
    changelog = "https://github.com/typst/typst/releases/tag/${src.rev}";
    description = "New markup-based typesetting system that is powerful and easy to learn";
    homepage = "https://github.com/typst/typst";
    license = lib.licenses.asl20;
    mainProgram = "typst";
    maintainers = with lib.maintainers; [
      drupol
      figsoda
      kanashimia
    ];
  };
}
```)] 
)

== nixpkgs - `cache.nixos.org`

- We do not want to build every package ourselves...

- Built packages are cached at #link("https://cache.nixos.org")[`cache.nixos.org`]

*Reminder*: If nothing changes, a derivation will have the same hash!

$==>$ If your change ('override') a package, it will automatically be built locally.


== Modules
- Your configuration consists of *modules*

#quote(attribution: link("https://nixos.wiki/wiki/NixOS_modules")[NixOS Wiki])[
  A module is a file containing a *Nix expression with a specific structure*. It *declares options* for other modules to define (give a value). It processes them and *defines options* declared in other modules.]

== Module structure - General (as function)
#text(size: 14pt)[
```nix
{ config, pkgs, lib, ... }: # inputs
{
  imports = [
    ./some-other-module.nix
  ];
  config = {
    option-from-any-other-module.enable = true;    
  };
  options = {
    my-option = lib.mkOption {
      type = lib.types.str;
      default = "some nice default value";
      description = "Set this option to a string of your choice";
    };
  };
}
```
]

== Module structure - compact (as function)
#text(size: 14pt)[
```nix
{ config, pkgs, lib, ... }: # inputs
{
  imports = [
    ./some-other-module.nix
  ];
  option-from-any-other-module.enable = true;    
}
```]

- This is how your `configuration.nix` is structured when you install NixOS

== configuration conflicts / merging
`module1.nix`
```nix
{ pkgs, ... }:
{
  environment.systemPackages = [ pkgs.vim ];
}
```

`module2.nix`
```nix
{ pkgs, ... }:
{
  environment.systemPackages = [ pkgs.emacs ];
}
```




== more on imports
- you often want to import everything from a subdirectory

#columns(2)[
  `directory structure`
  #text(size: 13pt)[
  ```
  .
  ├── submodules
  │   ├── module1.nix
  │   └── module2.nix
  └── top.nix
  ```]
  #colbreak()
  `top.nix`
  #text(size: 13pt)[
  ```nix
  imports = [
    ./submodules/module1.nix #ugly
    ./submodules/module2.nix
  ];

  ```]
]

== more on imports - modified

#columns(2)[
  `directory structure`
  #text(size: 13pt)[
  ```
 .
├── submodules
│   ├── default.nix
│   ├── module1.nix
│   └── module2.nix
└── top.nix 
  ```]
  `submodules/default.nix`
  #text(size: 13pt)[
  ```nix
  _ : {
    imports = [
      ./module1.nix
      ./module2.nix
    ];
  }
  ```]
  #colbreak()
  `top.nix`
  #text(size: 13pt)[
  ```nix
  imports = [
    ./submodules  
  ];
  ```]
]

= Flakes
== What are flakes?
- File-system-tree which contains a `flake.nix` at it's root
- Write nix expressions (packages!) in a sane way
- Proper dependency management
  - Improve reproducibility
- Provide unform naming scheme for declaring packages and their dependencies
== Dead-simple flake
```nix
{
  inputs.nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
  outputs = { self, nixpkgs, }: {
    packages.x86_64-linux.what-is-my-ip = nixpkgs.legacyPackages.x86_64-linux.callPackage ./what-is-my-ip.nix {};

    packages.x86_64-linux.default = self.packages.x86_64-linux.what-is-my-ip;
  };
}
```
== Let's build it!
```
❯ nix build 
❯ ls 
flake.lock flake.nix result what-is-my-ip.nix
```
- Hey, two new things!
  - flake.lock
  - result
== A further look at _result_
```
❯ ls -l
total 16
-rw-r--r-- 1 cr users 567 Oct 30 18:34 flake.lock
-rw-r--r-- 1 cr users 312 Oct 30 18:34 flake.nix
lrwxrwxrwx 1 cr users  57 Oct 30 18:35 result -> /nix/store/5jf23xp5nmqw8vhrjpgvbimrvd086v6z-what-is-my-ip/
-rw-r--r-- 1 cr users 247 Oct 30 18:34 what-is-my-ip.nix
```
- Symlink to the location of the build package in the nix store
- Easy to test our package, just run `./result/bin/what-is-my-ip`

== What's the _flake.lock_ doing there?
#columns(2,
[
#text(
size: 15pt, 
[```json
{
  "nodes": {
    "nixpkgs": {
      "locked": {
        "lastModified": 1729880355,
        "narHash": "sha256-RP+OQ6koQQLX5nw0NmcDrzvGL8HDLnyXt/jHhL1jwjM=",
        "owner": "nixos",
        "repo": "nixpkgs",
        "rev": "18536bf04cd71abd345f9579158841376fdd0c5a",
        "type": "github"
      },
      "original": {
        "owner": "nixos",
        "ref": "nixos-unstable",
        "repo": "nixpkgs",
        "type": "github"
      }
    },
    "root": {
      "inputs": {
        "nixpkgs": "nixpkgs"
      }
    }
  },
  "root": "root",
  "version": 7
}
```])])

== So, what's great about flakes exactly?
- Keep track of all inputs, lock them
- version-control your dependencies
  - something broken after a `nix flake update`? \
    $==>$ go back back a commit
  - provide packages easily

== NixOS flake structure
#columns(1)[
#text(12pt)[
```nix
{
  description = "My flake";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };
  outputs = inputs @ {self, nixpkgs, ...}: {
    inherit (nixpkgs) lib;
    nixosConfigurations = {
      laptop = lib.nixosSystem {
        system = "x86_64-linux";
        modules = [ 
          ./common-modules 
          ./laptop-specific.nix
        ];
      };
      pc = lib.nixosSystem { /* omitted */ };
  };
}
```
]]


  
= `nixos-rebuild`
- uses `Nix` to manage almost *everything* about your system  

- How to apply changes?

== `nixos-rebuild`
used to generate the new NixOS system based on the configuration

General idea

+ fetch packages / data 
+ build packages / generate new config files
+ apply changes

Remember derivations?
- *Your newly built NixOS system is also a derivation!*

== Consequence
- The old system (*derivation*) is kept!
- You messed something up? 
  - As long as it's not the bootloader you're fine.
  - The old system is still available in the bootloader
  
== Usage / Subcommands
`nixos-rebuild {switch | boot | test | build | build-vm | ...}`
#table(
  stroke: none,
  gutter: 0.2em,
  fill: (x, y) => if x == 0 and y == 0{
                  } else if y == 4 and x == 0 {
                    rgb("#f18a20")
                  } else if x == 0 or y == 0 { 
                    rgb("#f0f0f0")
                  } else {
                    rgb("#fAfAfA")
                  },
  columns: (1fr, 1fr, 1fr, 1fr, 1fr),
  align: (right, center, center, center, center),
  [          ],       [builds], [apply changes (switch)], [make default boot entry], [generate vm],
  [`build`   ],       [ X    ], [        ], [            ], [           ],
  [`boot`    ],       [ X    ], [        ], [ X          ], [           ],
  [`test`    ],       [ X    ], [ X      ], [            ], [           ],
  [`switch`  ],       [ X    ], [ X      ], [ X          ], [           ],
  [`build-vm`],       [ X    ], [        ], [            ], [ X         ],
  [  $dots$  ],       [      ], [        ], [            ], [           ]
)
*`build-vm`*: you still need to set up your system so that it can actually run a vm  
- $==>$ script in `result/bin/run-${name}-vm`
              


== Your future

#align(center)[
  #image("img/nixos-rebuild.jpg", height: 1fr)
]


== How to improve output of `nixos-rebuild`
`nh` - nix-helper

#columns(2,
  [ 
    #figure(
      caption: "dependency graph"
    )[
      #image("img/nh-dep-graph.png", width: 100%)
    ]

    #figure(
      caption: "version changes / install / uninstall"
    )[
      #image("img/nh-updates.png", width: 100%)
    ]
  ]  
)

== Upgrading
*without flakes*
  #local(highlights: (
    // (line: 0, start: 10, end: 10, fill: red),
))[
  ```bash
  nixos-rebuild switch --use-remote-sudo --upgrade
  ```
  or
  ```bash
  nh os switch
  ```
]
== Upgrading
*with `flakes`*

  ```bash
  nix flake update --flake '<path-to-flake>' # update flake inputs
  nixos-rebuild switch --use-remote-sudo --flake '<path-to-flake>#<config-name>' 
  ```
  or
  ```bash
  nh os switch --update
  ```

  

= Home Manager

== Manage your user with nix!
- Provides utilities to manage the *home directory*.
  - *dotfiles* (`~/.config`)
  - mime-apps
  - user systemd services
- can be installed standalone or integrated as NixOS module 
  - we recommend the *second* (unified) *approach* 

== How is this done? (especially with configs)
- NixOS derivations live in `/nix/store/`, not in `~` ...
  - *symlinks* to the rescue!

    $==>$ Creates symlinks upon *system activation*

- `home-manager` provides nix config options 

  $==>$ these are converted to their respective config formats
$$

== Symlinks, symlinks and more symlinks
#figure(
  caption: "Example from my laptop"
)[
  #image("img/home-manager-symlinks.png");
]


== Example (helix)

#columns(2, 
[
  #local(highlights: (
    (line: 2, start: 7, end: 22, fill: green),
  ))[
  #text(size: 14pt,
  ```nix
    home-manager.users.<myuser> = {
      programs.helix = {
        enable = true;
        settings = {
          editor = {
            auto-format = true;
            bufferline = "multiple";
            color-modes = true;
            completion-timeout = 5;
            cursorline = true;
            line-number = "relative";
            mouse = true;
            true-color = true;
    };};};}; # to save space
  ```)]
  #colbreak()
  ```toml
[editor]
auto-format        = true
bufferline         = "multiple"
color-modes        = true
completion-timeout = 5
cursorline         = true
line-number        = "relative"
mouse              = true
true-color         = true  
```
])



=== So why bother? I already have my dotfiles

#columns(2, 
[


  #show raw.where(): it => {
    show ".": "." + sym.zws
    it
  }  
  *NixOS configuration - `hyprland.nix`*
  
  
  #local(highlights: (
    (line: 11, start: 3, end: 3, fill: red),
  ))[
  #text(size: 14pt,
      ```nix
      home-manager.users.${username}.wayland.windowManager.hyprland.settings.monitors = 
      map (
        m: "${m.device},\
            ${toString m.resolution.x}x\
            ${toString m.resolution.y}@\
            ${toString m.refresh_rate},\
            ${toString m.position.x}x\
            ${toString m.position.y},\
            ${toString m.scale},\
            transform,\
            ${toString m.transform}"
      ) monitors # ignore the origin of this for now
      ```
    )
  ]
  #colbreak()

  *Generated `./config/hypr/hyprland.conf`*

  #text(size: 14pt,
    ```hyprland-config
    monitor=DP-2,2560x1440@143.998001,0x0,1.000000,transform,0
    monitor=HDMI-A-3,2560x1440@74.999900,2560x200,1.000000,transform,3
    monitor=HDMI-A-2,2560x1440@74.999900,-1440x200,1.000000,transform,1
    ```
  )

  - config language agnostic
  - allows global configuration options 
  - theming

]
)

= Toolbox

== CLI
=== `nix repl`
- search for packages
#figure(
  caption: "extra nix features in my config"
)[
  #image("img/nix-repl-pkgs.png", width: 60%)
]

=== `nix repl` 
- inspect configuration

#figure(
  caption: "extra nix features in my config"
)[
  #image("img/nix-repl.png", width: 100%)
]

=== `nix-shell`
Is used for development shells, but can also be useful to temporarily install software:

#figure(
  caption: "Quickly need a package?"
)[
  #image("img/nix-shell-tool.png", width: 80%)
]




== Websites
- #link("https://noogle.dev") - NixOS function search
- #link("https://search.nixos.org") - NixOS package search
- #link("https://home-manager-options.extranix.com")
- #link("https://wiki.nixos.org") - NixOS Wiki
  - *_Tip:_* don't use #link("https://nixos.wiki") (outdated)


= Wrappers

== nvf 
Configure everything in nix, even programs like neovim!
#text(size: 10pt, 
[#columns(2, [
```nix
config.vim = {
    viAlias = true;
    vimAlias = true;
    spellcheck.enable = true;
    lsp.formatOnSave = true;
    debugger = {
      nvim-dap = {
        enable = true;
        ui.enable = true;
      };
    };
    languages = {
      enableLSP = true;
      enableFormat = true;
      zig.enable = false;
      nix.enable = true;
      rust = {
        enable = true;
        crates.enable = true;
      };
    };
    autopairs.enable = true;
    treesitter.context.enable = true;
    autocomplete = {
      enable = true;
      type = "nvim-cmp";
    };
    filetree = {
      nvimTree = {
        enable = true;
      };
    };
    git = {
      enable = true;
      gitsigns.enable = true;
    };
```  
#link("https://github.com/NotAShelf/nvf")
  ])
])

== Docker!

#columns(2,
[
#text(
size: 13pt, 
[```nix
{ pkgs ? import <nixpkgs> {system = "x86_64-linux;" }}: let 
what-is-my-ip = import ./what-is-my-ip.nix {inherit pkgs;};
in
pkgs.dockerTools.buildImage {
  name = "what-is-my-ip-docker";
  config = {
    Cmd = [ "${what-is-my-ip}/bin/what-is-my-ip" ];
  };
}
```])])
- Works, but is it really better than using docker normally?
=== Easy layered Docker iamges

#columns(2,
[
#text(
size: 13pt, 
[```nix
{ pkgs ? import <nixpkgs> {system = "x86_64-linux;" }}: let 
what-is-my-ip = import ./what-is-my-ip.nix {inherit pkgs;};
in
pkgs.dockerTools.buildLayeredImage {
  name = "what-is-my-ip-docker";
  config = {
    Cmd = [ "${what-is-my-ip}/bin/what-is-my-ip" ];
  };
}
```])
#colbreak()
- puts each dependency into a seperate layer
- updating a single package/dependency?
  - only update that layer, and upload the part of the image which actually changed
- easy version controlling with git
])

= stylix

== Styling
- setting wallpaper
- unified color schemes

- can automatically *generate* color-scheme from wallpaper

== Downsides
- only supports a few tools for setting wallpapers
- can cause config conflicts if manual styling was applied

```nix
home-manager.users.<myuser>.programs.alacritty = {
  settings.colors.background = "#ffffff";
};
```

```nix
stylix = {
  enable = true;
  autoEnable = true;
  image = "my-wallpaper.jpeg";
};
```

== Dirty fix
#local(highlights: (
  (line: 1, start: 33, end: 38, fill: red),
))[
```nix
home-manager.users.<myuser>.programs.alacritty = {
  settings.colors.background = lib.mkForce "#ffffff";
};
```
]

```nix
stylix = {
  enable = true;
  autoEnable = true;
  image = "my-wallpaper.jpeg";
};
```
= disko

== What is disko?
- `NixOS` is declarative *BUT* disk partititoning still manual

  $==> $ automates this

- Defines *`disko.devices`*, a declarative description of your disks

- `btrfs` / `zfs` subvolumes can be quite annoying to set up

  - you forget what options you set the last time

- makes remote deployment easy with `nixos-anywhere`


== Example (just a snippet)


#text(size: 11pt, [#columns(2,
```nix
content = {
  type = "btrfs";
  extraArgs = ["-L" "nixos${cfg.name-suffix}" "-f"];
  subvolumes = {
    "/root" = {
      mountpoint = "/";
      mountOptions = ["subvol=root" "compress=zstd" "noatime"];
    };
    "/home" = {
      mountpoint = "/home";
      mountOptions = ["subvol=home" "compress=zstd" "noatime"];
    };
    "/nix" = {
      mountpoint = "/nix";
      mountOptions = ["subvol=nix" "compress=zstd" "noatime"];
    };
    "/log" = {
      mountpoint = "/var/log";
      mountOptions = ["subvol=log" "compress=zstd" "noatime"];
    };
    "/swap" = mkIf (cfg.swap-size != null) {
      mountpoint = "/swap";
      swap.swapfile.size = cfg.swap-size;
    };
  };
};
```
)])



== Downsides

#columns(2,
  [
  - Error handling? $==>$ bash script, so no
  - *no boot entry* unless `--write-efi-boot-entries` set 
  - Bad handling of *LUKS volumes* /disk names
    - *`/dev/by/by-partlabel`* instead of *`/dev/`* directly

     $==>$ this can make your *host* system *unbootable*
  - *deeply nested* configuration $==>$ $~ 10$ levels for me 
  - How often do you *really* partition a new system?
 
  
  #figure(
    image("img/disko.png", width:50%),
    caption: [
      *official* disko image \
      `github.com/nix-community/disko`
    ]
  ) <disko>

  ]
)

== Sources

- https://cohost.org/leftpaddotpy/post/798893-rechosting-a-banger
- https://nix.dev/tutorials/nix-language
- https://github.com/danth/stylix
- https://github.com/nix-community/disko
- https://github.com/NotAShelf/nvf
- https://fzakaria.com/2024/07/05/learn-nix-the-fun-way



\ \
*Feedback: #link("feedback@feedback.thealternative.ch")*
