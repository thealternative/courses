{
  inputs.nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
  outputs = {
    self,
    nixpkgs,
  }: {
    packages.x86_64-linux.what-is-my-ip = nixpkgs.legacyPackages.x86_64-linux.callPackage ./what-is-my-ip.nix {};

    packages.x86_64-linux.default = self.packages.x86_64-linux.what-is-my-ip;
  };
}
