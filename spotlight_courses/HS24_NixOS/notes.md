# Images

I am generating the images from the examples in ./examples using
https://github.com/charmbracelet/freeze

# Aufbau

1. Was ist NixOS?

- Aufbau

2. Unterschiede zu anderen Distros
3. Nix Sprache
4. Nix Package Manager

- Overrides

5. Meherere Systeme

6. Nixpkgs
7. Konfiguration Default config

- Neue nixos configuration
- Wo sachen finden? noogle.dev, search.nixos.org wiki.nixos.org, nixos.wiki
  schlecht

7. Module

- Wie aufgebaut, warum gut, Beispiele

8. Flakes

- Warum? -- Flake.lock und so
- Schema erklären
- Einfache Beispielflake
- Satpaper flake
- Komplizierte Flake

9. Home-Manger

- Vorteile & Nachteile
- Wie verwenden
- Module und Standalone
- home-manager-options.extranix.com
- Monitor config

10. Selber konfigurieren

- nix repl (nixosConfigurations)
- mit git tracken (oder svn)

11. devshells

- direnv

12. 1 Sprache für alle Programme

- Nvf
- Hyprland
- sway/i3
-

13. Selber was packagen

- mkDerivation
- buildRustPackage
- fetcher (passt der hash?) ((Zeigen, was das für komische fehler kommen))

14. Patchen von Binaries

15. Stylix

16. impermanence
17. Disko
18. nh

19. Vivado
20. nur

21. Agenix
22. Nixos-anywhere
23. Nixos-hardware

??. Kuhle Projekte

# Projekte

# Sources

https://nix.dev/tutorials/nix-language.html
