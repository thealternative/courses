#!/bin/bash -ue

# Colors
NC='\e[0m'
GR='\e[1;32m'
SP='\e[105m'

# This is the location of the BorgBase repository
TARGET=xxx.repo.borgbase.com:repo

# Options for borg create
BORG_OPTS="--stats --compression auto,zstd,6"

# Repo key passphrase
export BORG_PASSPHRASE="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

# It is better to just fail quickly instead of hanging.
export BORG_RELOCATED_REPO_ACCESS_IS_OK=no
export BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK=no

# Log Borg version
echo -e "${SP}$(borg --version)${NC}"

echo -e "${GR}$(date '+%Y-%m-%d %H:%M')${NC} Starting backup"

borg create $BORG_OPTS \
            $TARGET::{hostname}-{now} \
            ~/.gnupg \
            ~/.ssh \
            ~/.zshrc \
            ~/.zsh_custom \
            ~/.zshenv \
            ~/.bashrc \
            ~/.certs \
            ~/Documents \
            ~/Pictures \
2>&1 | tee ~/.backup_last.log

echo -e "${GR}$(date '+%Y-%m-%d %H:%M')${NC} Backup completed"

echo -e "${GR}$(date '+%Y-%m-%d %H:%M')${NC} Starting to prune old archives"

borg prune -v --list \
           --glob-archives='{hostname}-*' \
           --keep-daily=7 \
           --keep-weekly=4 \
           --keep-monthly=-1 $TARGET \
2>&1 | tee -a ~/.backup_last.log

echo -e "${GR}$(date '+%Y-%m-%d %H:%M')${NC} Prune completed"

#echo -e "${GR}$(date '+%Y-%m-%d %H:%M')${NC} Compacting repository"
#
#borg -v compact --cleanup-commits $TARGET 2>&1 | tee -a ~/.backup_last.log
#
#echo -e "${GR}$(date '+%Y-%m-%d %H:%M')${NC} Compacting completed"

cat <<EOF > ~/.backup_last.mail
To: iyanmv@gmail.com
From: Borg <me@iyanmv.com>
Subject: [BorgBase] Backup report $(date '+%Y-%m-%d %H:%M')

EOF
cat ~/.backup_last.log >> ~/.backup_last.mail
cat ~/.backup_last.mail | msmtp -a iyanmv iyanmv@gmail.com
