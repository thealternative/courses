# Backups in GNU/Linux
## Planning and automating backups with Borg

This folder contains the slides and scripts presented during the "Spotlight 2 - Backups" that took place at ETH on 18 November 2022.

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
