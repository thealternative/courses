# Additional resources

* Terminal emulator
* Shell
	* bash (standard)
	* csh
	* fsh
* statusbar
	* polybar
	* lemonbar
	* dzen2 (write your own)
* panel
	* xfce4-panel
	* tint2
* conky
* ImageMagick
* neofetch
* i3lock
* Programming languages
	* bash
	* python
	* ruby
* pywal
* rofi
* dmenu
* neofetch
* redshift (xrandr gamma correction)

