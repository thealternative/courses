---
author:
 - Jannis Piekarek
title: Introduction to Linux
---

#

##

###

\bigtext{You are already using Linux!}

###

\begin{figure} \centering\includegraphics[height=0.9\textheight]{img/cardreader.jpg} \end{figure}

###

\begin{figure}
\centering
\includegraphics[height=0.9\textheight]{img/ticketautomat.jpg}
\end{figure}

### 

\begin{figure}
\centering
\includegraphics[height=0.9\textheight]{img/android_logo.jpg}
\end{figure}

###

\begin{figure}
\centering
\includegraphics[height=0.9\textheight]{img/internet.jpg}
\end{figure}

###

\bigtext{Desktop Linux}\
\vspace{1em}

# Why Linux?

## But Why Linux?

###

\bigtext{Desktop Linux}\
But why Linux?

###

\bigtext{Freedom}

### Freedom

\begin{itemize}
        \item The freedom to run linux as you wish, for any purpose
        \item The freedom to study how linux, and change it so it does your
  computing as you wish
        \item The freedom to redistribute copies so you can help your neighbor
        \item The freedom to distribute copies of your modified versions to others
      \end{itemize}

### Freedom

\begin{multicols}{2}[]

Customize!

\vspace{0.5em}

\includegraphics[height=0.38\textheight]{img/customize01.png}
\includegraphics[height=0.38\textheight]{img/customize02.png}

\columnbreak

\color{white}.
\color{black}

\vspace{0.5em}

\includegraphics[height=0.38\textheight]{img/plasma_screenshot.png}
\includegraphics[height=0.38\textheight]{img/Xfce_screenshot.png}

\end{multicols}

### Freedom

Say no to un-uninstallable software!
\begin{figure}
\centering
\includegraphics[height=0.4\textheight]{img/uninstall_edge.png}
\end{figure}

### Privacy

\begin{multicols}{2}[
Say no to telemetry!
]

\begin{figure}
\includegraphics[height=0.4\textheight]{img/telemetry.png}
\end{figure}

\columnbreak

\begin{figure}
\includegraphics[height=0.3\textheight]{img/telemetry_ubuntu.png}
\end{figure}

\end{multicols}
### No Ads

\begin{multicols}{2}[]
\begin{figure}
\centering
\includegraphics[height=0.2\textheight]{img/startmenu-ads.jpg}
\end{figure}
\begin{figure}
\centering
\includegraphics[height=0.3\textheight]{img/ads3.png}
\end{figure}
\columnbreak
\begin{figure}
\centering
\includegraphics[height=0.5\textheight]{img/ads2.jpg}
\end{figure}
\end{multicols}

### Easy software installation & management

\begin{figure}
\centering
\includegraphics[height=0.8\textheight]{img/software-center.png}
\end{figure}

### Easy updates, whenever YOU want!

\begin{figure}
\centering
\includegraphics[height=0.3\textheight]{img/windowsupdate.png}
\end{figure}

### Use powerful tools!


\begin{multicols}{3}[]
\begin{figure}
\centering
\includegraphics[height=0.3\textheight]{img/bash.png}
\end{figure}

\columnbreak

\begin{figure}
\centering
\includegraphics[height=0.3\textheight]{img/gnu.png}
\end{figure}

\columnbreak

\begin{figure}
\centering
\includegraphics[height=0.3\textheight]{img/vim.png}
\end{figure}
\end{multicols}

###

\bigtext{But what does Linux mean?}

### Linux, what does it mean?

* A *Linux system* is a system that uses the Linux *Kernel*.

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{img/linux_kernel.pdf}
\end{figure}

### 

\bigtext{Linux}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{img/puzzlepiece.pdf}
\end{figure}

### 

\bigtext{GNU/Linux}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{img/linux_kernel_blank.pdf}
\end{figure}

### System components

* Kernel \soft{the core of your OS that manages all other programs and your devices}

* Utilities \soft{the core utilities of your system in our case often GNU}

* Bootloader \soft{a tiny program that runs first when you power your PC and takes care of starting up the kernel}

* Init system \soft{a program that takes care of starting and stopping background tasks}

* Package manager \soft{a program that allows you to install other programs}

* Graphics framework \soft{a basic software that allows other programs to draw stuff on the screen}

* Shell \soft{the program that powers the console}

### System components you care about

* Desktop Environment and/or Window Manager
* Editor
* Browser
* Mail client

### My system

\begin{figure}
\centering
\includegraphics[height=0.8\textheight]{img/my_system.pdf}
\end{figure}

### Standard Ubuntu system

\begin{figure}
\centering
\includegraphics[height=0.8\textheight]{img/ubuntu_system.pdf}
\end{figure}

# Linux Concepts

##

### <!-- WMP vs VLC -->

\color{white}.....
\color{black}
\includegraphics[width=0.4\textwidth]{img/wmp.jpg}
\raisebox{4em}{\bigtext{vs.}}
\includegraphics[width=0.3\textwidth]{img/vlc-logo.png}

### <!-- WMP vs VLC -->

\color{white}.....
\color{black}
\includegraphics[width=0.4\textwidth]{img/wmp.jpg}
\raisebox{4em}{\bigtext{vs.}}
\includegraphics[width=0.4\textwidth]{img/vlc.pdf}

### <!-- WMP vs VLC -->

\includegraphics[width=0.4\textwidth]{img/wmp.jpg}
\raisebox{4em}{\bigtext{vs.}}
\includegraphics[width=0.4\textwidth]{img/vlcdeps.pdf}

### The Unix Philosophy

\bigtext{Do one thing, and do it right}

### Package Managers

* Your best friend when it comes to dependency management
* Installs and removes software
* Updates all your programs with one click

### Package Managers

\begin{figure}
\centering
\includegraphics[height=0.85\textheight]{img/package-manager-blank.pdf}
\end{figure}

### Refresh

\begin{figure}
\centering
\includegraphics[height=0.85\textheight]{img/package-manager-refresh.pdf}
\end{figure}

### Upgrade or Install

\begin{figure}
\centering
\includegraphics[height=0.85\textheight]{img/package-manager-upgrade.pdf}
\end{figure}

###

\bigtext{So you want to use Linux}

###

\begin{figure}
\centering
\includegraphics[height=0.85\textheight]{img/logos.png}
\end{figure}

###

\bigtext{But how to choose?}

### Standard Ubuntu system

\begin{figure}
\centering
\includegraphics[height=0.8\textheight]{img/ubuntu_system.pdf}
\end{figure}

### Distros

\begin{multicols}{2}[]
Ubuntu (and forks)
\begin{figure}
\centering
\includegraphics[height=0.5\textheight]{img/ubuntu_logo.png}
\end{figure}

\columnbreak

Fedora
\begin{figure}
\centering
\includegraphics[height=0.5\textheight]{img/fedora_logo.png}
\end{figure}

\end{multicols}

### Outlook: Arch Linux
\begin{minipage}{0.6\textwidth} 
  \begin{itemize}
    \item Keeping to the bare minimum
    \item Provide just the basics, let the user choose all components
    \item Make a system that is truly yours
  \end{itemize}
\end{minipage}\begin{minipage}{0.4\textwidth}
  \includegraphics[width=\textwidth]{img/arch_logo.png}
\end{minipage}

###

\bigtext{Desktop Environments}

### KDE

\begin{figure}
\centering
\includegraphics[height=0.8\textheight]{img/kde-plasma.jpg}
\end{figure}

### Gnome

\begin{figure}
\centering
\includegraphics[height=0.8\textheight]{img/gnome-desktop.png}
\end{figure}

### Xfce

\begin{figure}
\centering
\includegraphics[height=0.7\textheight]{img/xfce-desktop.png}
\end{figure}

### Cinnamon

\begin{figure}
\centering
\includegraphics[height=0.8\textheight]{img/cinnamon.jpg}
\end{figure}

