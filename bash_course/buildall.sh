#!/bin/bash

echo 'Guide'
(
    cd guide
    ./build.sh | sed 's/^/- /'
)
echo 'Presentation'
(
    cd pres
    ./build.sh | sed 's/^/- /'
)
echo 'Exercise sheet'
(
    cd exercisesheet
    ./build.sh | sed 's/^/- /'
)
