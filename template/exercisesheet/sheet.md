# About this course

This document provides complementary exercises that you can solve as you go
through the bash guide. For each exercise, the sections of the course that are
required to solve it are listed. Some also rely on external tutorials.

You don’t need to solve all the exercises. You can pick the ones you find
interesting. Some exercises, though, are labelled *recommended*.  These either
provide you with useful ’building blocks’ for future exercises and scripts of
your own, or test some basic knowledge that you will need.

We recommend you start with the easy exercises, and once you feel confident,
continue with the more challenging ones. If you run into problems, you can ask
our staff members for help.

## Recommended chapters

Every exercise has a list of "requirements" that designate chapters which you
will need to solve that specific exercise. However, there are some chapters
that we recommend you read first, before you start any exercise, as they
contain important knowledge that will prevent you from making simple "beginner
mistakes".

Please read at least

* 2 Commands and Arguments
* 3 Variables and Parameters
* 5.1 Expansion and Quotes
* 5.2 Expansion Order

before commencing with the exercises.

# Exercises

## Make your bash prompt fancy

Your bash prompt is what you see in a terminal when you enter commands. It can
be modified by setting the variable `PS1`. Try setting `PS1` to something
different from your terminal.

In this exercise, you will customize your bash prompt. There are some things
you need to know first, though:

* Within your command prompt, you can use several *escape sequences* that will
  be replaced by some interesting information:

    | Sequence | Decoding |
    |----------|----------|
    |`\d`   |  the date  in  "Weekday  Month  Date"  format (e.g., "Tue May 26") |
    |`\e`   |  an ASCII escape character (033) |
    |`\h`   |  the hostname up to the first '.' |
    |`\H`   |  the hostname |
    |`\j`   |  the  number of jobs currently managed by the shell |
    |`\n`   |  a newline |
    |`\s`   |  the  name  of  the shell |
    |`\t`   |  the current time in 24-hour HH:MM:SS format | 
    |`\T`   |  the current time in 12-hour HH:MM:SS format |
    |`\@`   |  the current time in 12-hour am/pm format |
    |`\u`   |  the username of the current user |
    |`\v`   |  the version of bash (e.g., 2.00) | 
    |`\w`   |  the current working directory | 
    |`\W`   |  the  basename  of the current working directory |
    |`\!`   |  the history number of this command | 
    |`\#`   |  the command number of this command |
    |`\$`   |  if you're logged in as root, this prints a #, otherwise a $ | 
    |`\\`   |  a backslash |

* You can use the following codes to make your prompt colorful:

    | Code           | Color |
    |----------------|-------| 
    | `\[\e[0;30m\]` | black |
    | `\[\e[0;34m\]` | blue |
    | `\[\e[0;32m\]` | green |
    | `\[\e[0;36m\]` | cyan |
    | `\[\e[0;31m\]` | red |
    | `\[\e[0;35m\]` | purple |
    | `\[\e[0;33m\]` | brown |
    | `\[\e[0;37m\]` | gray |
    | `\[\e[1;30m\]` | dark gray |
    | `\[\e[1;34m\]` | light blue |
    | `\[\e[1;32m\]` | light green |
    | `\[\e[1;36m\]` | light cyan |
    | `\[\e[1;31m\]` | light red |
    | `\[\e[1;35m\]` | light purple |
    | `\[\e[1;33m\]` | yellow |
    | `\[\e[1;37m\]` | white |
    | `\[\e[0m\]`    | back to normal |

Now that you know this, write a script that adjusts your bash prompt to your
liking.

**Requirements:**

* 3 Variables and Parameters

**Tips:**

* This endeavour becomes a lot easier if you define variables for the color
  codes you need. Be aware that you need to quote these codes properly.
* Set your `PS1` to something you like. Use the variables you defined to change
  the color.
* At the end of your prompt, you should use the code for *back to normal*. If
  you don't do that, the output of your commands will be colored as well.
* Your prompt will change when you run the script from a terminal. If you want
  to make the changes permanent, you can execute your script from the
  `~/.bashrc` file--this file is executed whenever you open a terminal.

## Web radio

Write a script that, when executed, starts playing your favorite webradio
station.

**Requirements:**

* 6.3 Conditional Blocks

**Tips:**

* You will need a webradio station whose stream is accessible via an IP
  address. You can use the [Xatworld radio
  search](https://www.xatworld.com/radio-search/) to find such a station.
* Once you have the IP address, find out how to play it from the console.
* Extend your script to take an argument which is the name of a station, and
  make it play the station that you provided as argument. The script would have
  a predefined list of stations it can play.

## Image resizer

Write a script that creates resized copies of all the pictures in the current
folder.

**Requirements:**

* 4 Globbing
* 6.4 Conditional Loops

**Tips:**

* We recommend downloading some pictures from the web and putting them in a
  folder to experiment with (to prevent you from deleting your own pictures).
* Have a look at ImageMagick’s `convert` command. Find out how to use it and
  resize a single image.
* Make sure your `convert` command only resizes images that are larger than the
  target size.
* `convert` can only operate on one image at a time. Your script will have to
  call it repeatedly.
* The resized copies should be stored in a separate, newly created subfolder.
* Find a way to iterate over all the files in a folder and call `convert` for
  each.
* Don’t worry about files that are not images---`convert` will ignore them
  anyway.

## Assignment-fetcher

Write a script that downloads all pdf files from a given website (for example,
to get all your course assignments at once).

This exercise is a bit more involved, and is best solved using regular
expressions (regex). Regex are not covered in this course, but you can find
great online resources, like this [Quick Start
Tutorial](http://www.regular-expressions.info/quickstart.html). Regex can be
used in combination with `grep`, for example.

**Requirements:**

* 3 Variables and Parameters
* 5.6 Command Substitution
* 6.5 Choices
* 7.1 Command-line Arguments
* 7.4 Pipes

**Tips:**

* Find a way to isolate all URLs that end in ’.pdf’ from a website.  You could
  use `grep` or `sed`.
* If you want to use `grep`, have a look at its `-o` and `-e` options.
* Depending on your course website, the URLs might be absolute (start with
  http) or relative (start with a / or a directory). Your script should be able
  to handle both.
* When you get relative URLs, you will have to prepend the base URL. Google for
  HTML relative URLs for more information.
* You can get the base url from the full url using `grep` with regular expressions.
* Process all the URLs such that you end up with absolute URLs only.
* Optionally, filter the URLs for a certain keyword that you can give your
  script as an argument. That way, you can prevent your script from downloading
  unrelated PDFs.
* Download all the files from the URL list to the current directory.
* Course slides at ETH are sometimes password protected. Have a look at wget’s
  `-user` and `-password` options. Unfortunately, these don’t work with the
  official nethz login, so if your course website uses that, you can’t use a
  bash script.
